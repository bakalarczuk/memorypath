﻿using UnityEditor;
using System.IO;
using UnityEngine;
using System.Collections;

public class CreateAssetBundles
{
    [MenuItem("Assets/Build AssetBundles/All")]
    static void BuildAllAssetBundlesAll()
    {
        BuildAllAssetBundlesAndroid();
        BuildAllAssetBundlesiOS();
    }

    private const string _publishDirectory = "Assets/Publish";
    private const string _assetBundleDirectoryAndroid = "Assets/AssetBundles/Android";
    private const string _assetBundleDirectoryiOS = "Assets/AssetBundles/iOS";
    private const string _extensionAndroid = ".android";
    private const string _extensioniOS = ".ios";

    [MenuItem("Assets/Build AssetBundles/Android")]
    static void BuildAllAssetBundlesAndroid()
    {
        BuildAllAssetBundlesGeneric(_assetBundleDirectoryAndroid, _extensionAndroid, BuildTarget.Android);
    }

    [MenuItem("Assets/Build AssetBundles/iOS")]
    static void BuildAllAssetBundlesiOS()
    {
        BuildAllAssetBundlesGeneric(_assetBundleDirectoryiOS, _extensioniOS, BuildTarget.iOS);
    }

    static void BuildAllAssetBundlesGeneric(string assetBundleDirectoryTargetPlatform, string extension, BuildTarget targetPlatform)
    {
        Debug.Log("Started Creating AssetBundle for " + extension);
        string assetBundleDirectory = assetBundleDirectoryTargetPlatform;
        if (Directory.Exists(assetBundleDirectory))
        {
            CleanDirectory(assetBundleDirectory);
            Directory.Delete(assetBundleDirectory);
        }

        Directory.CreateDirectory(assetBundleDirectory);

        if (!Directory.Exists(_publishDirectory))
        {
            Directory.CreateDirectory(_publishDirectory);
        }

        //CleanDirectory(assetBundleDirectory);

        AssetBundleManifest assetBundleManifest = BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, targetPlatform);
        string filename = assetBundleDirectory + "/" + assetBundleManifest.GetAllAssetBundles()[0];
        string targetfilename = _publishDirectory + "/" + assetBundleManifest.GetAllAssetBundles()[0];
        string targetfilenameAndExtension = targetfilename + extension;
        if (File.Exists(targetfilename))
        {
            File.Delete(targetfilename);
        }
        if (File.Exists(targetfilenameAndExtension))
        {
            File.Delete(targetfilenameAndExtension);
        }
        File.Move(filename, targetfilename);
        File.Move(targetfilename, Path.ChangeExtension(targetfilename, extension));

        //Refresh porject folders to show changes
        AssetDatabase.Refresh();
        Debug.Log("Finished creating Assetbundle for " + extension + ", your file was saved as " + targetfilename + targetfilenameAndExtension);
    }

    public static void CleanDirectory(string directoryPath)
    {
        DirectoryInfo directory = new DirectoryInfo(directoryPath);

        foreach (FileInfo file in directory.GetFiles())
        {
            file.Delete();
        }
        foreach (DirectoryInfo dir in directory.GetDirectories())
        {
            dir.Delete(true);
        }
    }
}
