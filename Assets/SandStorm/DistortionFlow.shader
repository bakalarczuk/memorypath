﻿Shader "Custom/DistortionFlow" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MinAlpha ("MinAlpha", Range(0,1)) = 1.0
		_MaxAlpha("MaxAlpha", Range(0,1)) = 1.0
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		[NoScaleOffset] _FlowMap ("Flow (RG, A noise)", 2D) = "black" {}
		_UJump ("U jump per phase", Range(-0.25, 0.25)) = 0.25
		_VJump ("V jump per phase", Range(-0.25, 0.25)) = -0.25
		_Tiling ("Tiling", Float) = 1
		_Speed ("Speed", Float) = 1
		_FlowStrength ("Flow Strength", Float) = 1
		_FlowOffset ("Flow Offset", Float) = 0
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags {"Queue" = "Transparent" "RenderType"="Transparent" }

		CGPROGRAM
		#pragma surface surf Standard alpha 
		#pragma target 3.0

		#include "Flow.cginc"

		sampler2D _MainTex, _FlowMap;
		float _UJump, _VJump, _Tiling, _Speed, _FlowStrength, _FlowOffset;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _MinAlpha;
		float _MaxAlpha;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			float2 flowVector = tex2D(_FlowMap, IN.uv_MainTex).rg * 2 - 1;
			flowVector *= _FlowStrength;
			float noise = tex2D(_FlowMap, IN.uv_MainTex).a;
			float time = _Time.y * _Speed + noise;
			float2 jump = float2(_UJump, _VJump);

			float3 uvwA = FlowUVW(
				IN.uv_MainTex, flowVector, jump,
				_FlowOffset, _Tiling, time, false
			);
			float3 uvwB = FlowUVW(
				IN.uv_MainTex, flowVector, jump,
				_FlowOffset, _Tiling, time, true
			);

			fixed4 texA = tex2D(_MainTex, uvwA.xy) * uvwA.z;
			fixed4 texB = tex2D(_MainTex, uvwB.xy) * uvwB.z;

			float transparency = 0.0f;

			if (_MinAlpha > _MaxAlpha)
				transparency = 1.0f;        //If the minimum is greater than the maximum, return a default value

			float cap = _MaxAlpha - _MinAlpha;    //Subtract the minimum from the maximum
			float rand = tex2D(_FlowMap, float2(noise, noise) + _Time.x).r * cap + _MinAlpha;    //Make the texture UV random (add time) and multiply noise texture value by the cap, then add the minimum back on to keep between min and max 
			transparency = rand;    //Return this value

			//float transparency = Random(_MinAlpha, _MaxAlpha, float2(noise, noise));
			fixed4 c = (texA + texB) * _Color;
			o.Albedo = c.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = transparency;
		}

		float RandomTest() 
		{
			return 1.0f;
		}

		/*float Random(float min, float max, float2 uv)    //Pass this function a minimum and maximum value, as well as your texture UV
		{
			if (min > max)
				return 1;        //If the minimum is greater than the maximum, return a default value

			float cap = max - min;    //Subtract the minimum from the maximum
			float rand = tex2D(_NoiseTex, uv + _Time.x).r * cap + min;    //Make the texture UV random (add time) and multiply noise texture value by the cap, then add the minimum back on to keep between min and max 
			return rand;    //Return this value
		}*/

		ENDCG
	}

	FallBack "Diffuse"
}