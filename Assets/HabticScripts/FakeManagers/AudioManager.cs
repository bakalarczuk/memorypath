﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Habtic.Managers
{
    public class AudioManager : MonoBehaviour
    {
        public static AudioManager Instance;

        [SerializeField] private AudioMixer _audioMixer;
        [SerializeField] private AudioMixerGroup _musicAudioMixerGroup;
        [SerializeField] private AudioMixerGroup _sFXAudioMixerGroup;

        private const string MUSIC_VOLUME = "MUSIC_VOLUME";
        private const string SFX_VOLUME = "SFX_VOLUME";
        private const string MUSIC_PARAMETER = "MusicVol";
        private const string SFX_PARAMETER = "SFXVol";

        public float MusicVolume
        {
            get
            {
                return PlayerPrefs.GetFloat(MUSIC_VOLUME, 1.0f);
            }
            set
            {
                PlayerPrefs.SetFloat(MUSIC_VOLUME, Mathf.Max(0.0001f, value));
                UpdateAudioMixer();
            }
        }

        public float SfxVolume
        {
            get
            {
                return PlayerPrefs.GetFloat(SFX_VOLUME, 1.0f);
            }
            set
            {
                PlayerPrefs.SetFloat(SFX_VOLUME, Mathf.Max(0.0001f, value));
                UpdateAudioMixer();
            }
        }

        public AudioMixerGroup MusicAudioMixerGroup
        {
            get
            {
                return _musicAudioMixerGroup;
            }
        }

        public AudioMixerGroup SFXAudioMixerGroup
        {
            get
            {
                return _sFXAudioMixerGroup;
            }
        }

        private void Awake()
        {
            if (Instance)
            {
                string className = GetType().Name;
                Debug.Log($"There already is an instance of singleton [{className}].");
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }

        private void Start()
        {
            UpdateAudioMixer();
        }

        private void UpdateAudioMixer()
        {
            _audioMixer.SetFloat(MUSIC_PARAMETER, Mathf.Log10(MusicVolume) * 20.0f);
            _audioMixer.SetFloat(SFX_PARAMETER, Mathf.Log10(SfxVolume) * 20.0f);
        }

    }
}
