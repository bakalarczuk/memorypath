﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Fog"
{
	Properties
	{
		[Header(Textures and color)]
		[Space]
		_MainTex ("Fog texture", 2D) = "white" {}
		[NoScaleOffset] _Mask ("Mask", 2D) = "white" {}
		_Color ("Color", color) = (1., 1., 1., 1.)
		[Space(10)]

		[Header(Behaviour)]
		[Space]
		_ScrollDirX ("Scroll along X", Range(-1., 1.)) = 1.
		_ScrollDirY ("Scroll along Y", Range(-1., 1.)) = 1.
		_Speed ("Speed", float) = 1.
		_Distance ("Transparency", Range(0., 1.)) = 1.
		_MiddleBot ("Bottom Middle", Range(0., 0.5)) = 0.5
		_MiddleTop ("Top MIddle", Range(0.5, 1.)) = 1.

        _FlowMap ("Flow Map", 2D) = "white" {}
        _FlowSpeed ("Flow Speed", float) = 0.05
	}

	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#include "Flow.cginc"

			struct v2f {
				float4 pos : SV_POSITION;
				fixed4 vertCol : COLOR0;
				float2 uv : TEXCOORD0;
				float2 texcoord : TEXCOORD1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Distance;
			float _MiddleBot;
			float _MiddleTop;
			sampler2D _Mask;
			float _Speed;
			fixed _ScrollDirX;
			fixed _ScrollDirY;
			fixed4 _Color;

            sampler2D _FlowMap;
            float _FlowSpeed;

			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);;
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.texcoord = v.texcoord;
				o.vertCol = v.color;
				return o;
			}


			fixed4 frag(v2f i) : SV_Target
			{
				

				fixed4 _ColorTop =  fixed4(0,0,0,0);
				fixed4 _ColorMidBot =  fixed4(0,0,0,1);
				fixed4 _ColorMid =  fixed4(0,0,0,1);
				fixed4 _ColorMidTop =  fixed4(0,0,0,1);
				fixed4 _ColorBot =  fixed4(0,0,0,0);
				
				float2 uv = i.uv + fixed2(_ScrollDirX, _ScrollDirY) * _Speed * _Time.x;
				fixed4 col = tex2D(_MainTex, uv) * _Color * i.vertCol;


				fixed4 c = lerp(_ColorBot, _ColorMidBot, i.texcoord.x / _MiddleBot) * step(i.texcoord.x, _MiddleBot);
				c += lerp(_ColorMidBot, _ColorMidTop, (i.texcoord.x - _MiddleBot) /(_MiddleTop - _MiddleBot) ) * step(_MiddleBot,i.texcoord.x) * step(i.texcoord.x,_MiddleTop);
				c += lerp(_ColorMidTop, _ColorTop, (i.texcoord.x - _MiddleTop) / (1 - _MiddleTop)) * step(_MiddleTop, i.texcoord.x);
            
				col.a *= tex2D(_Mask, i.texcoord).r;
				col.a =  c.a;


				float3 flowDir = tex2D(_FlowMap, i.texcoord) * 2.0f - 1.0f;
                flowDir *= _FlowSpeed;

                float phase0 = frac(_Time[1] * 0.5f + 0.5f);
                float phase1 = frac(_Time[1] * 0.5f + 1.0f);

                half3 tex0 = tex2D(_MainTex, uv + flowDir.xy * phase0);
                half3 tex1 = tex2D(_MainTex, uv + flowDir.xy * phase1);

                float flowLerp = abs((0.5f - phase0) / 0.5f);
                half3 finalColor = lerp(tex0, tex1, flowLerp);

                fixed4 c2 = float4(finalColor, 1.0f);
				c2*=col;
				return c2;
			}
			
			ENDCG
		}
	}
}