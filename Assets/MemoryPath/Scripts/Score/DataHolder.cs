﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Habtic.Games.MemoryPath
{
    class DataHolder
    {
        public static int highscore = 0;
        public static int Score = 0;
        public static bool PlayedIntroLevel = false;
        public static bool ShowLevelMessage = true;
        public static int personalAverageDifficulty = 5;
        public static List<int> successfullGamesThisSession = new List<int>();
        public static string game_difficulty = "Medium";
    }
}

