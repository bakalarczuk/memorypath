﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Habtic.Games.MemoryPath
{
    public class MainMenuController : MonoBehaviour
    {
        public TMP_Text highscore, score;
        public Button tutorialButton;
        public Button[] difficultyButtons;
        public TextMeshProUGUI tutorialButtonText;
        public TextMeshProUGUI[] difficultyTexts;
        public TMP_FontAsset[] fonts;

        private LevelManager _levelManager;
        private Level _level;
        private bool _playIntro = false;

        void Start()
        {
            _levelManager = LevelManager.Instance;
            _level = Level.Instance;
        }
        private void OnEnable()
        {
            highscore.text = DataHolder.highscore.ToString();
            score.text = DataHolder.Score.ToString();

            if (DataHolder.PlayedIntroLevel == false)
            {
                SelecTutorial();
            }
            else
            {
                SetDifficulty(GameManager.Instance.CurrentLevelDifficulty);
            }

            MusicManager.Instance.PlayMusic(MusicTrack.Title);
        }

        public void StartGame()
        {
            if (!_playIntro)
            {
                _levelManager.LoadScene(1);
            }
            else
            {
                DataHolder.PlayedIntroLevel = true;
                _levelManager.LoadScene(3);
            }
        }

        public void SelecTutorial()
        {
            UpdateButtons(3);
        }

        public void DifficultyEasy()
        {
            SetDifficulty(LevelDifficulty.EASY);
        }

        public void DifficultyMedium()
        {
            SetDifficulty(LevelDifficulty.MEDIUM);
        }

        public void DifficultyHard()
        {
            SetDifficulty(LevelDifficulty.HARD);
        }

        private void SetDifficulty(LevelDifficulty levelDifficulty)
        {
            GameManager.Instance.CurrentLevelDifficulty = levelDifficulty;
            GameManager.Instance._level.SetDifficulty(levelDifficulty);

            UpdateButtons((int)levelDifficulty);

            DataHolder.game_difficulty = GameManager.Instance.CurrentLevelDifficulty.ToString();
        }

        private void UpdateButtons(int selected)
        {
            if (selected > 2)
            {
                _playIntro = true;
                tutorialButton.OnSelect(null);
                tutorialButtonText.font = fonts[0];
                for (int i = 0; i < difficultyButtons.Length; i++)
                {
                    difficultyButtons[i].OnDeselect(null);
                    difficultyTexts[i].font = fonts[1];
                }
            }
            else
            {
                _playIntro = false;
                tutorialButton.OnDeselect(null);
                tutorialButtonText.font = fonts[1];
                for (int i = 0; i < difficultyButtons.Length; i++)
                {
                    if (i != (int)GameManager.Instance.CurrentLevelDifficulty)
                    {
                        difficultyButtons[i].OnDeselect(null);
                        difficultyTexts[i].font = fonts[1];
                    }
                    else
                    {
                        difficultyButtons[i].OnSelect(null);
                        difficultyTexts[i].font = fonts[0];
                    }
                }
            }
        }
    }
}
