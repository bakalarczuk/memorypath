using RotaryHeart.Lib.SerializableDictionary;
using System;
using UnityEngine;
using Habtic.Managers;

namespace Habtic.Games.MemoryPath
{
    public enum MusicTrack { Title, Gameplay }

    [Serializable] public class MusicDictionary : SerializableDictionaryBase<MusicTrack, AudioClip> { }

    public class MusicManager : SingletonWithNoAutomaticInstance<MusicManager>
    {
        [SerializeField] private MusicDictionary _tracks;

        private AudioSource _audioSource;

        public void Awake()
        {
            if (_audioSource == null) GetAudio();
        }

        public void GetAudio()
        {
            _audioSource = GetComponent<AudioSource>();
            _audioSource.outputAudioMixerGroup = AudioManager.Instance.MusicAudioMixerGroup;
        }

        public void PlayMusic(MusicTrack chosenTrack)
        {
            if (_audioSource == null) GetAudio();
            AudioClip clip = null;
            if (_tracks.TryGetValue(chosenTrack, out clip))
            {
                if (_audioSource.clip != clip)
                {
                    _audioSource.clip = clip;
                    _audioSource.Play();
                }
            }
        }

    }
}
