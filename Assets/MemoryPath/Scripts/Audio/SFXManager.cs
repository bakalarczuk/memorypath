﻿using UnityEngine;
using System;
using RotaryHeart.Lib.SerializableDictionary;

namespace Habtic.Games.MemoryPath
{
    public enum SFXTrack { Click, Correct, Incorrect, Go, Footsteps, Sandstorm, Wind }

    [Serializable] public class SFXDictionary : SerializableDictionaryBase<SFXTrack, AudioClip> { }

    public class SFXManager : SingletonWithNoAutomaticInstance<SFXManager>
    {
        [SerializeField] private SFXDictionary _tracks;

        [SerializeField] private GameObject _SFXPrefab;

        public void PlaySFX(SFXTrack chosenTrack)
        {
            SFXClip sfxClip = null;
            foreach (Transform child in transform)
            {
                if (child.gameObject.activeSelf == false)
                {
                    sfxClip = child.GetComponent<SFXClip>();
                    break;
                }
            }

            if (sfxClip == null)
            {
                if (_SFXPrefab != null)
                    sfxClip = Instantiate(_SFXPrefab, transform).GetComponent<SFXClip>();
                else
                    return;
            }

            AudioClip clip = null;
            if (_tracks.TryGetValue(chosenTrack, out clip))
                sfxClip.PlayClip(clip);
        }

        public void Click()
        {
            PlaySFX(SFXTrack.Click);
        }
    }


}
