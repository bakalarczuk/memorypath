﻿using UnityEngine;
using Habtic.Managers;

namespace Habtic.Games.MemoryPath
{
    public class SFXClip : MonoBehaviour
    {
        private AudioSource _audioSource;

        public AudioSource AudioSource { get { return _audioSource; } }

        public void Awake()
        {
            if (_audioSource == null) GetAudio();
        }

        public void GetAudio()
        {
            _audioSource = GetComponent<AudioSource>();
            _audioSource.outputAudioMixerGroup = AudioManager.Instance.SFXAudioMixerGroup;
        }

        public void Update()
        {
            if (!_audioSource.isPlaying)
            {
                gameObject.SetActive(false);
            }
        }

        public void PlayClip(AudioClip clip)
        {
            if (_audioSource == null) GetAudio();
            gameObject.SetActive(true);
            _audioSource.clip = clip;
            _audioSource.Play();
        }

        public void Pause()
        {
            _audioSource.Pause();
        }
    }
}
