﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using Habtic.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI.Extensions;

namespace Habtic.Games.MemoryPath
{
    public class GameManager : Singleton<GameManager>
    {
        public delegate void LevelStateChanged(LevelStates levelState);
        public static event LevelStateChanged OnLevelStateChanged;

        public LevelStates LevelState
        {
            get
            {
                return _levelState;
            }
            set
            {
                // Debug.Log("GameLevelState: " + value.ToString());
                _levelState = value;
                if (OnLevelStateChanged != null)
                {
                    OnLevelStateChanged(value);
                }
            }
        }
        private LevelStates _levelState;

        public delegate void GameSetup();
        public static event GameStarted OnGameSetup;

        public delegate void GameStarted();
        public static event GameStarted OnGameStarted;

        public delegate void InputChekced();
        public static event GameStarted OnInputChecked;
        public delegate void InputCorrect();
        public static event GameStarted OnCorrectInput;
        public delegate void InputIncorrect();
        public static event GameStarted OnIncorrectInput;

        public delegate void LifesChanged(int lifes);
        public static event LifesChanged OnLifesChanged;

        public delegate void ScoreChanged(int score, int realscore);
        public static event ScoreChanged OnScoreChanged;

        public delegate void MessageBroadcasted(BroadcastMessageEventArgs args);
        public static event MessageBroadcasted OnMessageBroadcasted;

        public delegate void NextLevel(NextLevelEventArgs args);
        public static event NextLevel OnNextLevel;

        public delegate void ChallengeOver();
        public static event ChallengeOver OnChallengeOver;

        public delegate void GameOver();
        public static event GameOver OnGameOver;

        public delegate void PauseState(bool isPaused);
        public static event PauseState OnPauseStateChanged;
        public delegate void NewBoard();
        public static event NewBoard OnNewBoard;


        #region PROPERTIES
        public Game game;
        public Button menuButton;
        [HideInInspector] public LevelDifficulty CurrentLevelDifficulty = LevelDifficulty.MEDIUM;
        private GameObject _selected;

        [SerializeField]
        private LevelQuestion _questionPanel;
        [SerializeField]
        private Timer _timer;
        [SerializeField]
        private TimerPanel _timerPanel;
        [SerializeField]
        private Board _board;
        [SerializeField]
        private GameObject _rightWrongIndicator;
        [SerializeField]
        private UILineRenderer _line;
        [SerializeField]
        private Transform _footprints;
        [SerializeField]
        private Animator leavesAnimator;

        public Timer GameTimer { get; }

        private float leavesNormalSpeed = 2;
        private float leavesStormSpeed = 4;


        public Board GameBoard { get { return _board; } }
        public UILineRenderer Line { get { return _line; } }

        [SerializeField]
        private SandstormScript dustStorm;

        public SandstormScript DustStorm { get { return dustStorm; } }

        [SerializeField] private LifesHUD _lifesHUD;

        private Tile wrongTile;

        private float answerTime = 5;
        private float previewTime = 2;

        public float AnswerTime { get { return answerTime; } set { answerTime = value; } }

        public bool Paused
        {
            get
            {
                return _paused;
            }
            private set
            {
                _paused = value;
                if (OnPauseStateChanged != null)
                {
                    OnPauseStateChanged(value);
                }
            }
        }
        public int TotalLifes { get { return _totalLifes; } set { _totalLifes = value; _lifesHUD.Initialize(TotalLifes); } }
        public int Lifes
        {
            get
            {
                return _lifes;
            }
            set
            {
                _lifes = value;

                // Notify listeners
                if (OnLifesChanged != null)
                    OnLifesChanged(_lifes);

                if (_lifes <= 0)
                {
                    _lifes = 0;

                    DataHolder.Score = Score;

                    // GameOver
                    LevelState = LevelStates.gameOver;
                }

            }
        }
        public int Score
        {
            get
            {
                return _score;
            }
            set
            {
                int oldScore = _score;
                _score = value;

                if (OnScoreChanged != null)
                {
                    OnScoreChanged(oldScore, value);
                }
            }
        }

        public LevelQuestion QuestionPanel { get { return _questionPanel; } }
        #endregion PROPERTIES

        public Level _level;
        private int _totalLifes = 5;
        private int _lifes = 5;
        private int _score = 0;
        private bool _paused = false;
        [SerializeField]
        private float previewTimer = 0;

        private Tile currentTile;

        private bool makeDustStorm = false;

        private bool pathChecking = false;

        private int counter = 0;

        void Awake()
        {
            _level = Level.Instance;
        }

        void Start()
        {
            InputManager.Instance.OnTouchUp += OnTouchUp;
            InputManager.Instance.OnDragged += OnDragged;
        }

        void OnEnable()
        {
            _timer.OnTimerFinished += TimerGameOver;
            _timer.OnPercentageLeftChanged += _timerPanel.UpdateTimer;
            OnGameSetup += Initialize;
            OnGameOver += GameOverMessage;
            SandstormScript.OnSandStormHalfWay += OnSandStormHalfWay;
            SandstormScript.OnSandStormStop += OnSandStormStop;
            OnChallengeOver += ChallengeOverMessage;
            OnLevelStateChanged += InitLevelStateChanged;
            LevelState = LevelStates.setup;
        }

        void OnDisable()
        {
            _timer.OnTimerFinished -= TimerGameOver;
            _timer.OnPercentageLeftChanged -= _timerPanel.UpdateTimer;
            OnGameSetup -= Initialize;
            OnGameOver -= GameOverMessage;
            SandstormScript.OnSandStormHalfWay -= OnSandStormHalfWay;
            SandstormScript.OnSandStormStop -= OnSandStormStop;
            OnChallengeOver -= ChallengeOverMessage;
            OnLevelStateChanged -= InitLevelStateChanged;
        }

        void OnDestroy()
        {
            InputManager.Instance.OnTouchUp -= OnTouchUp;
            InputManager.Instance.OnDragged -= OnDragged;
        }

        public void LeaveGame()
        {
            DustStorm.StopStorm();
            _board.RemoveSnakes();
            _board.ResetBoard();
        }

        void Update()
        {
            if ((LevelState == LevelStates.preview || LevelState == LevelStates.resume) && makeDustStorm && !FlashController.Instance.flashing)
            {

                if (_board.snakes.Count > 0)
                {
                    if (_board.SnakesCanMove)
                    {
                        foreach (Snake s in _board.snakes)
                        {
                            s.StartMoving();
                        }
                        _board.tramp.GetComponent<Hero>().cloud.SetActive(true);

                        _board.SnakesCanMove = false;
                    }
                }

                if (!Paused) { previewTimer += Time.deltaTime; }

                if (previewTimer >= previewTime)
                {
                    dustStorm.StartStorm(leavesAnimator, leavesStormSpeed);
                    makeDustStorm = false;
                }
            }

            if (pathChecking)
            {
                if (LevelState == LevelStates.play || LevelState == LevelStates.resume)
                {
                    float distance = 0.05f; //on which distance you want to switch to the next waypoint
                    Vector3 direction = Vector3.zero;
                    //get the vector from your position to current waypoint
                    direction = _board.GetPath[counter].transform.position - _board.tramp.transform.position;
                    //check our distance to the current waypoint, Are we near enough?
                    if (direction.magnitude < distance)
                    {
                        if (_board.GetPath[counter].tileType == TileType.GOOD)
                        {
                            counter++;
                            if (counter == _board.GetPath.Count)
                            {
                                pathChecking = false;
                                _board.tramp.GetComponent<Animator>().SetBool("walk", false);
                                _board.GetStartTile = _board.GetPath[counter - 1];
                                _board.ClearPath();
                                counter = 0;
                                _board.CanDrawPath = true;
                                _timer.ResumeTimer();
                            }
                        }
                        else if (_board.GetPath[counter].tileType == TileType.WRONG)
                        {
                            if (_board.tramp.transform.position == _board.GetPath[counter].transform.position)
                            {
                                pathChecking = false;
                                _board.tramp.GetComponent<Animator>().SetBool("falling", true);
                                _board.GetPath[counter].SetTile(TileType.PIT);
                                _timer.PauseTimer();
                                LevelState = LevelStates.IncorrectInput;
                                _line.enabled = false;
                            }
                        }
                        else if (_board.GetPath[counter].tileType == TileType.END)
                        {
                            if (_board.tramp.transform.position == _board.GetPath[counter].transform.position)
                            {
                                pathChecking = false;
                                _board.tramp.GetComponent<Animator>().SetBool("walk", false);
                                LevelState = LevelStates.correctInput;
                                _line.enabled = false;
                            }
                        }

                        Transform footprints = Instantiate(_footprints, _board.transform, false);
                        footprints.localEulerAngles = _board.tramp.transform.localEulerAngles;
                        footprints.position = _board.tramp.transform.position;
                        footprints.localScale = new Vector3(_board.tileSize / 2, _board.tileSize / 2, _board.tileSize / 2);
                        SFXManager.Instance.PlaySFX(SFXTrack.Footsteps);
                    }
                    float zFactor = 0;
                    if (direction.normalized.x > 0.5f || direction.normalized.x < -0.5f) zFactor = -90 * direction.normalized.x;
                    else if (direction.normalized.y < -0.5f) zFactor = 180;
                    else zFactor = 0;

                    _questionPanel.gameObject.SetActive(false);

                    _board.tramp.transform.localEulerAngles = new Vector3(0, 0, zFactor);
                    if (counter < _board.GetPath.Count)
                        _board.tramp.transform.position = Vector3.MoveTowards(_board.tramp.transform.position, _board.GetPath[counter].transform.position, Time.deltaTime * 1.5f);
                }
            }
        }

        private void OnSandStormHalfWay()
        {
            for (int i = 0; i < _board.AllTiles.Count; i++)
            {
                _board.AllTiles[i].ResetTile();
            }
            _board.tramp.GetComponent<Hero>().cloud.SetActive(false);

            previewTimer = 0;
            _board.treasure.SetActive(true);
            _board.RemoveSnakes();
        }

        private void OnSandStormStop()
        {
            LevelState = LevelStates.play;
            _timer.TimerReset();
            _timer.StartTimer(10, true);
            leavesAnimator.speed = leavesNormalSpeed;
        }

        private void InitLevelStateChanged(LevelStates levelState)
        {
            switch (levelState)
            {
                case LevelStates.setup:
                    OnGameSetup?.Invoke();
                    break;
                case LevelStates.play:
                    OnGameStarted?.Invoke();
                    break;
                case LevelStates.gameOver:
                    OnGameOver?.Invoke();
                    break;
                case LevelStates.challengeend:
                    OnChallengeOver?.Invoke();
                    break;
                case LevelStates.correctInput:
                    OnInputChecked?.Invoke();
                    OnCorrectTouch();
                    break;
                case LevelStates.IncorrectInput:
                    OnInputChecked?.Invoke();
                    OnIncorrectTouch();
                    break;
                case LevelStates.pause:
                    Pause();
                    break;
                case LevelStates.resume:
                    Resume();
                    break;
                default:
                    break;
            }
        }

        void Initialize()
        {
            TotalLifes = _level.TotalLifes;
            Lifes = _totalLifes;
            Score = 0;
            previewTimer = 0;
            _rightWrongIndicator.transform.LeanScale(Vector3.zero, 0);
            if (!DataHolder.ShowLevelMessage) { LevelStart(); }
        }

        public void LevelStart()
        {
            if (OnNewBoard != null)
            {
                OnNewBoard();
            }
            _board.ResetBoard();
            counter = 0;
            _rightWrongIndicator.transform.LeanScale(Vector3.one, 0.2f);
            _board.CanDrawPath = true;
            LevelState = LevelStates.preview;
            makeDustStorm = true;
            _board.StartNewLevel(_level);
            SFXManager.Instance.PlaySFX(SFXTrack.Go);
        }

        private void OnDragged(YTouchEventArgs e)
        {
            if (LevelState == LevelStates.play || LevelState == LevelStates.resume)
            {
                if (_board.CanDrawPath)
                    DrawPath(e);
            }
        }

        private void OnTouchUp(YTouchEventArgs e)
        {
            if ((LevelState == LevelStates.play || LevelState == LevelStates.resume) && _board.GetPath.Count > 1 && _board.CanDrawPath)
            {
                _board.CanDrawPath = false;
                if (IsCorrectPath(_board.GetPath))
                    CheckPath(_board.GetPath);
                else
                {
                    //LevelState = LevelStates.IncorrectInput;
                    _line.enabled = false;
                    _board.ClearPath();
                    counter = 0;
                    _board.CanDrawPath = true;
                }
            }

        }

        private void OnCorrectTouch()
        {
            Debug.Log("Correct touch detected");
            OnCorrectInput?.Invoke();
            _line.enabled = false;
            float _currentLevel = _level.CurrentLevel;
            _level.CorrectInARowCounter = _level.CorrectInARowCounter + 1;
            _level.ChallengeCounter = _level.ChallengeCounter + 1;
            _level.CorrectCounter = _level.CorrectCounter + 1;

            _timer.TimerReset();

            AddScore(_level.ScorePerCorrectAnswer);
            CheckChallege();

            if (_currentLevel == _level.CurrentLevel)
            {
                LevelState = LevelStates.play;
            }
        }

        private void OnIncorrectTouch()
        {
            Debug.Log("Incorrect touch detected");
            OnIncorrectInput?.Invoke();
            _line.enabled = false;
            _level.CorrectInARowCounter = 0;
            _level.IncorrectCounter = _level.IncorrectCounter + 1;
            _level.ChallengeCounter = _level.ChallengeCounter + 1;

            _timer.TimerReset();

            RemoveLife();
            CheckChallege();

            if (Lifes > 0)
            {
                LevelState = LevelStates.play;
            }
        }

        public void Pause()
        {
            Paused = true;
            LeanTween.pauseAll();

            if (_board.tramp)
            {
                _board.tramp.GetComponent<Animator>().speed = 0f;
            }
        }

        public void Resume()
        {
            Paused = false;
            LeanTween.resumeAll();

            if (_board.tramp)
            {
                _board.tramp.GetComponent<Animator>().speed = 1f;
            }
        }

        public void AddScore(int score)
        {
            Score = Score + score;

            if (_level.CorrectInARowCounter >= _level.NextLevel && _level.ChallengeCounter < _level.TotalChallenges)
            {
                IncreaseLevel();
            }
        }


        public void CheckChallege()
        {
            if (_level.ChallengeCounter >= _level.TotalChallenges)
            {
                LevelState = LevelStates.challengeend;
            }
        }


        public void RemoveLife()
        {
            Lifes = _lifes - 1;
        }

        private void IncreaseBoardSize()
        {
            if (_level.CurrentLevel % _level.NextBoardSizeLevel == 0 && _level.BoardSize < _board.boardSizes.Length)
            {
                _level.BoardSize++;
                _board.BoardSize = _board.boardSizes[_level.BoardSize];
            }
        }

        private void IncreaseLevel()
        {
            _level.CurrentLevel++;

            IncreaseBoardSize();

            // Notify listeners
            if (OnNextLevel != null)
            {
                OnNextLevel(new NextLevelEventArgs
                {
                    ScorePerCorrectAnswer = _level.ScorePerCorrectAnswer,
                    TotalLifes = _level.TotalLifes
                });
            }
        }

        private void BroadCastMessage(MessageType type, string message)
        {
            if (OnMessageBroadcasted != null)
                OnMessageBroadcasted(new BroadcastMessageEventArgs { Type = type, Message = message });
        }

        public void disableMenu(bool value)
        {
            menuButton.interactable = !value;
        }

        private void GameOverMessage()
        {
            GameOverController.Instance.LoadScoreMenu();
        }

        private void ChallengeOverMessage()
        {
            GameOverController.Instance.LoadScoreMenu(false);
        }

        private void TimerGameOver()
        {
            LevelState = LevelStates.IncorrectInput;
            _line.enabled = false;
        }

        private void DrawPath(YTouchEventArgs touchEventArgs)
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            pointerData.position = touchEventArgs.TouchStart;
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);
            if (results != null && results.Count <= 0) { return; }
            Tile tile = results[0].gameObject.transform.parent.GetComponent<Tile>();
            if (tile != null)
            {
                if (!_board.PathContains(tile) && (tile.tileType == TileType.GOOD || tile.tileType == TileType.WRONG || tile.tileType == TileType.END))
                {
                    if (_board.GetPath.Count == 0) { _board.AddTile(tile); }

                    if (_board.GetPath[0] != _board.GetStartTile) { _board.ClearPath(); return; }

                    if (_board.GetPath.Count > 0)
                    {
                        if (_board.GetPath[_board.GetPath.Count - 1].neighbours.Contains(tile))
                        {
                            _board.AddTile(tile);
                        }
                    }
                    if (_board.GetPath.Count > 1)
                    {
                        _line.Points = _board.GetPath.GetPoints();
                        _line.enabled = true;
                    }
                }
            }
        }

        private bool IsCorrectPath(List<Tile> path)
        {
            if (path[0] == _board.GetStartTile)// && path[path.Count - 1] == _board.GetEndTile)
            {
                return true;
            }
            return false;
        }

        private void CheckPath(List<Tile> path)
        {
            _timer.PauseTimer();
            counter = 0;
            pathChecking = true;
            _board.tramp.GetComponent<Animator>().SetBool("walk", true);
        }

        private void SetWrongTile(Tile tile)
        {
            wrongTile = tile;
        }

        void OnApplicationFocus(bool hasFocus)
        {
            Paused = !hasFocus;
        }

        void OnApplicationPause(bool pauseStatus)
        {
            Paused = pauseStatus;
        }


    }


    public class NextLevelEventArgs : EventArgs
    {
        public int ScorePerCorrectAnswer { get; set; }
        public int TotalLifes { get; set; }
    }

    public enum MessageType
    {
        INFO,
        WARNING,
        POSITIVE,
        NEGATIVE
    }

    public enum LevelStates
    {
        none,
        setup,
        play,
        correctInput,
        IncorrectInput,
        nextLevel,
        replay,
        gameOver,
        challengeend,
        preview,
        pause,
        resume
    }

    public class BroadcastMessageEventArgs : EventArgs
    {
        public MessageType Type { get; set; }
        public string Message { get; set; }
    }

}
