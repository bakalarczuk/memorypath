﻿using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;
using Habtic.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI.Extensions;


namespace Habtic.Games.MemoryPath
{
    public class IntroLevelManager : Singleton<IntroLevelManager>
    {
        [SerializeField]
        private Game _game;

        private string TutorialPreview, TutorialDrawCorrectPath, TutorialDrawIncorrectPath, TutorialTimeOut, TutorialGame;

        public delegate void LevelStateChanged(LevelStates levelState);
        public static event LevelStateChanged OnLevelStateChanged;
        public delegate void NewTutorial();
        public static event NewTutorial OnNewTutorial;


        public LevelStates LevelState
        {
            get
            {
                return _levelState;
            }
            set
            {
                // Debug.Log("GameLevelState: " + value.ToString());
                _levelState = value;
                if (OnLevelStateChanged != null)
                {
                    OnLevelStateChanged(value);
                }
            }
        }
        private LevelStates _levelState;

        public delegate void GameSetup();
        public static event GameStarted OnGameSetup;

        public delegate void GameStarted();
        public static event GameStarted OnGameStarted;

        public delegate void InputChekced();
        public static event GameStarted OnInputChecked;
        public delegate void InputCorrect();
        public static event GameStarted OnCorrectInput;
        public delegate void InputIncorrect();
        public static event GameStarted OnIncorrectInput;


        public delegate void LifesChanged(int lifes);
        public static event LifesChanged OnLifesChanged;

        public delegate void ScoreChanged(int score, int realscore);
        public static event ScoreChanged OnScoreChanged;

        public delegate void MessageBroadcasted(BroadcastMessageEventArgs args);
        public static event MessageBroadcasted OnMessageBroadcasted;

        public delegate void NextLevel(NextLevelEventArgs args);
        public static event NextLevel OnNextLevel;

        public delegate void GameOver();
        public static event GameOver OnGameOver;

        public delegate void PauseState(bool isPaused);
        public static event PauseState OnPauseStateChanged;

        #region PROPERTIES
        public Button menuButton;
        private GameObject _selected;

        [SerializeField]
        private IntroLevelQuestion _questionPanel;
        [SerializeField] private Timer _timer;
        [SerializeField] private TimerPanel _timerPanel;

        [SerializeField]
        private MessagePanelIntro _introEndMassagePanel;

        [SerializeField]
        private AnswerIndicator _notifications;

        [SerializeField]
        private Board _board;

        public Board GameBoard { get { return _board; } }

        [SerializeField]
        private SandstormScript dustStorm;

        public SandstormScript DustStorm { get { return dustStorm; } }

        private Tile wrongTile;

        private float answerTime = 5;
        private float previewTime = 2;

        [SerializeField]
        private float previewTimer = 0;

        [SerializeField]
        private UILineRenderer _line;

        [SerializeField]
        private TutorialSwipeIndicator _swipeIndicator;

        private Tile currentTile;

        private bool makeDustStorm = false;

        private bool pathChecking = false;

        private int counter = 0;

        [SerializeField]
        private GameObject _rightWrongIndicator;

        [SerializeField]
        private Transform _footprints;

        [SerializeField]
        private Animator leavesAnimator;
        [SerializeField] private TutorialLifesHUD _lifesHUD;

        private float leavesNormalSpeed = 2;
        private float leavesStormSpeed = 6;

        private LTDescr _tweenM;
        public Vector2[] path;
        public Vector2[] wrongPath;

        private int tutorialType = 1;

        public bool Paused
        {
            get
            {
                return _paused;
            }
            private set
            {
                _paused = value;
                if (OnPauseStateChanged != null)
                {
                    OnPauseStateChanged(value);
                }
            }
        }
        public int TotalLifes { get { return _totalLifes; } set { _totalLifes = value; } }
        public int Lifes
        {
            get
            {
                return _lifes;
            }
            set
            {
                _lifes = value;

                // Notify listeners
                if (OnLifesChanged != null)
                    OnLifesChanged(_lifes);

                if (_lifes <= 0)
                {
                    _lifes = 0;

                    DataHolder.Score = Score;

                    // GameOver
                    LevelState = LevelStates.gameOver;
                }

            }
        }
        public int Score
        {
            get
            {
                return _score;
            }
            set
            {
                int oldScore = _score;
                _score = value;

                if (OnScoreChanged != null)
                {
                    OnScoreChanged(oldScore, value);
                }
            }
        }
        #endregion PROPERTIES

        private Level _level;
        private int _totalLifes = 5;
        private int _lifes = 5;
        private int _score = 0;
        private bool _paused = false;

        public bool TutorialEnd { get; set; }

        void Awake()
        {
            _level = Level.Instance;
            TutorialPreview = _game.LocalizedStrings["game_level_instruction_preview"];
            TutorialDrawCorrectPath = _game.LocalizedStrings["game_level_instruction_correct_path"];
            TutorialDrawIncorrectPath = _game.LocalizedStrings["game_level_instruction_incorrect_path"];
            TutorialTimeOut = _game.LocalizedStrings["game_level_instruction_timeout"];
            TutorialGame = _game.LocalizedStrings["game_level_instruction_game"];
            leavesAnimator.speed = leavesNormalSpeed;
        }

        void OnEnable()
        {
            OnGameSetup += Initialize;
            OnLevelStateChanged += InitLevelStateChanged;
            InputManager.Instance.OnTouchUp += OnTouchUpTutorial;
            InputManager.Instance.OnDragged += OnDraggedTutorial;
            SandstormScript.OnSandStormHalfWay += OnSandStormHalfWay;
            SandstormScript.OnSandStormStop += OnSandStormStop;
            _timer.OnTimerFinished += OnTimerFinishedTutorials;
            _timer.OnPercentageLeftChanged += _timerPanel.UpdateTimer;
            FlashController.OnFadeIn += OnFadeIn;
            LevelState = LevelStates.setup;
        }

        void OnDisable()
        {
            OnGameSetup -= Initialize;
            OnLevelStateChanged -= InitLevelStateChanged;
            InputManager.Instance.OnTouchUp -= OnTouchUpTutorial;
            InputManager.Instance.OnDragged -= OnDraggedTutorial;
            SandstormScript.OnSandStormHalfWay -= OnSandStormHalfWay;
            SandstormScript.OnSandStormStop -= OnSandStormStop;
            _timer.OnTimerFinished -= OnTimerFinishedTutorials;
            _timer.OnPercentageLeftChanged -= _timerPanel.UpdateTimer;
            FlashController.OnFadeIn -= OnFadeIn;
            _board.ResetBoard();
        }

        void OnDestroy()
        {
            // InputManager.Instance.OnSwiped -= OnSwipedCheck;
        }

        void Update()
        {
            if (LevelState == LevelStates.preview && makeDustStorm && !FlashController.Instance.flashing)
            {
                if (_board.snakes.Count > 0)
                {
                    if (_board.SnakesCanMove)
                    {
                        foreach (Snake s in _board.snakes)
                        {
                            s.StartMoving();
                        }
                        _board.tramp.GetComponent<Hero>().cloud.SetActive(true);

                        _board.SnakesCanMove = false;
                    }
                }
                previewTimer += Time.deltaTime;
                if (previewTimer >= previewTime)
                {
                    dustStorm.StartStorm(leavesAnimator, leavesStormSpeed);
                    makeDustStorm = false;
                }
            }

            if (pathChecking)
            {
                if (LevelState == LevelStates.play || LevelState == LevelStates.resume)
                {
                    float distance = 0.05f; //on which distance you want to switch to the next waypoint
                    Vector3 direction = Vector3.zero;
                    //get the vector from your position to current waypoint
                    direction = _board.GetPath[counter].transform.position - _board.tramp.transform.position;
                    //check our distance to the current waypoint, Are we near enough?
                    if (direction.magnitude < distance)
                    {
                        // if (counter < _board.GetPath.Count - 1)
                        // {
                        if (_board.GetPath[counter].tileType == TileType.GOOD)
                        {
                            counter++;
                            if (counter == _board.GetPath.Count)
                            {
                                pathChecking = false;
                                _board.tramp.GetComponent<Animator>().SetBool("walk", false);

                                _board.GetStartTile = _board.GetPath[counter - 1];
                                _board.ClearPath();
                                counter = 0;
                                _board.CanDrawPath = true;
                            }
                        }
                        else if (_board.GetPath[counter].tileType == TileType.WRONG)
                        {
                            if (_board.tramp.transform.position == _board.GetPath[counter].transform.position)
                            {
                                pathChecking = false;
                                _board.tramp.GetComponent<Animator>().SetBool("falling", true);
                                _board.GetPath[counter].SetTile(TileType.PIT);
                                if (tutorialType == 2)
                                {
                                    StartCoroutine(StepEndRoutine("game_notify_good_answer", true, tutorialType));
                                    _lifesHUD.AddLifes(-1);
                                }
                                else
                                    StartCoroutine(StepEndRoutine("game_notify_wrong_answer", false, tutorialType));
                                _line.enabled = false;
                                _swipeIndicator.StopSwipeTween();
                            }
                        }
                        else if (_board.GetPath[counter].tileType == TileType.END)
                        {
                            if (_board.tramp.transform.position == _board.GetPath[counter].transform.position)
                            {
                                pathChecking = false;
                                _board.tramp.GetComponent<Animator>().SetBool("walk", false);
                                if (tutorialType == 1)
                                    StartCoroutine(StepEndRoutine("game_notify_good_answer", true, tutorialType));
                                else
                                    StartCoroutine(StepEndRoutine("game_notify_wrong_answer", false, tutorialType));

                                _line.enabled = false;
                                _swipeIndicator.StopSwipeTween();
                            }
                        }
                        else if (_board.GetPath[counter] == _board.GetEndTile)
                        {
                            if (_board.tramp.transform.position == _board.GetPath[counter].transform.position)
                            {
                                pathChecking = false;
                                _board.tramp.GetComponent<Animator>().SetBool("walk", false);
                                _board.CanDrawPath = true;
                                StartCoroutine(StepEndRoutine("game_notify_good_answer", true, tutorialType));
                                _line.enabled = false;
                            }
                        }
                        Transform footprints = Instantiate(_footprints, _board.transform, false);
                        footprints.localEulerAngles = _board.tramp.transform.localEulerAngles;
                        footprints.position = _board.tramp.transform.position;
                        footprints.localScale = new Vector3(_board.tileSize / 2, _board.tileSize / 2, _board.tileSize / 2);
                        SFXManager.Instance.PlaySFX(SFXTrack.Footsteps);

                    }
                    float zFactor = 0;
                    if (direction.normalized.x > 0.5f || direction.normalized.x < -0.5f) zFactor = -90 * direction.normalized.x;
                    else if (direction.normalized.y < -0.5f) zFactor = 180;
                    else zFactor = 0;

                    _board.tramp.transform.localEulerAngles = new Vector3(0, 0, zFactor);
                    if (counter < _board.GetPath.Count)
                        _board.tramp.transform.position = Vector3.MoveTowards(_board.tramp.transform.position, _board.GetPath[counter].transform.position, Time.deltaTime * 1.5f);
                }
            }
        }

        private void OnSandStormHalfWay()
        {
            for (int i = 0; i < _board.AllTiles.Count; i++)
            {
                _board.AllTiles[i].ResetTile();
            }

            previewTimer = 0;
            _board.treasure.SetActive(true);
            foreach (Snake s in _board.snakes)
            {
                Destroy(s.gameObject);
            }
            _board.snakes.Clear();
            _board.tramp.GetComponent<Hero>().cloud.SetActive(false);
            _questionPanel.SetQuestionText(tutorialType == 1 ? TutorialDrawCorrectPath : tutorialType == 2 ? TutorialDrawIncorrectPath : tutorialType == 3 ? TutorialTimeOut : string.Empty);

        }

        private void OnSandStormStop()
        {
            LevelState = LevelStates.play;

            List<Tile> goodPath = PathFinder.Instance.GetPath(_board.GetStartTile, _board.GetEndTile, false);
            goodPath.Insert(0, _board.GetEndTile);

            path = goodPath.GetPointsReversed();

            List<Tile> path1 = PathFinder.Instance.GetPath(_board.GetStartTile, _board.BadTiles[0], true);
            path1.Remove(_board.BadTiles[0]);
            List<Tile> path2 = PathFinder.Instance.GetPath(_board.BadTiles[0], _board.GetEndTile, true);
            path2.Insert(0, _board.GetEndTile);

            path2.AddRange(path1);
            wrongPath = path2.GetPointsReversed();

            if (tutorialType < 3)
            {
                _swipeIndicator.Counter = 0;
                _swipeIndicator.MoveOnPath(tutorialType == 1 ? path : tutorialType == 2 ? wrongPath : null);
            }
            else
            {
                _timerPanel.ShowIndicator(true);
                _timer.TimerReset();
                _timer.StartTimer(5, true);
            }

            leavesAnimator.speed = leavesNormalSpeed;
        }

        private void InitLevelStateChanged(LevelStates levelState)
        {
            switch (levelState)
            {
                case LevelStates.setup:
                    OnGameSetup?.Invoke();
                    break;
                case LevelStates.preview:
                    OnGameStarted?.Invoke();
                    break;
                case LevelStates.gameOver:
                    OnGameOver?.Invoke();
                    break;
                case LevelStates.correctInput:
                    OnInputChecked?.Invoke();
                    OnCorrectTouch();
                    break;
                case LevelStates.IncorrectInput:
                    OnInputChecked?.Invoke();
                    OnIncorrectTouch();
                    break;
                case LevelStates.pause:
                    break;
                case LevelStates.resume:
                    ;
                    break;
                default:
                    break;
            }
        }

        void Initialize()
        {
            _level.CurrentLevel = 1;
            _level.WrongTilesCount = 1;
            TotalLifes = 1;
            Lifes = 1;
            Score = 0;
            _introEndMassagePanel.Hide();
            _questionPanel.Hide();
            _rightWrongIndicator.SetActive(false);
            TutorialEnd = false;
        }

        public void LevelStart()
        {
            StartTutorialOne();
        }

        public void StartTutorialOne()
        {
            OnNewTutorial?.Invoke();

            _board.gameObject.SetActive(true);
            pathChecking = false;
            _board.CanDrawPath = true;
            tutorialType = 1;
            _rightWrongIndicator.SetActive(true);
            makeDustStorm = true;
            _board.StartNewLevel(_level, true);
            SFXManager.Instance.PlaySFX(SFXTrack.Go);
        }

        public void StartTutorialTwo()
        {
            OnNewTutorial?.Invoke();

            _board.gameObject.SetActive(true);
            pathChecking = false;
            _board.CanDrawPath = true;
            tutorialType = 2;
            _rightWrongIndicator.SetActive(true);
            makeDustStorm = true;
            _board.StartNewLevel(_level, true);
            SFXManager.Instance.PlaySFX(SFXTrack.Go);
            _lifesHUD.Initialize(TotalLifes);
        }
        public void StartTutorialThree()
        {
            OnNewTutorial?.Invoke();

            _board.gameObject.SetActive(true);
            pathChecking = false;
            _board.CanDrawPath = true;
            tutorialType = 3;
            _rightWrongIndicator.SetActive(true);
            makeDustStorm = true;
            _board.StartNewLevel(_level, true);
            SFXManager.Instance.PlaySFX(SFXTrack.Go);
            _lifesHUD.Initialize(TotalLifes);
        }

        private void OnFadeIn()
        {
            _questionPanel.SetQuestionText(TutorialPreview);
        }
        private void OnTimerFinishedTutorials()
        {
            if (LevelState == LevelStates.play || LevelState == LevelStates.resume)
            {
                _timer.OnTimerFinished -= OnTimerFinishedTutorials;
                _timer.PauseTimer();
                _lifesHUD.AddLifes(-1);
                StartCoroutine(StepEndRoutine("game_notify_time_out", true, 3));
            }
        }

        private void OnDraggedTutorial(YTouchEventArgs e)
        {
            if (LevelState == LevelStates.play || LevelState == LevelStates.resume)
            {
                if (_board.CanDrawPath)
                    DrawPath(e);
            }
        }

        private void OnTouchUpTutorial(YTouchEventArgs e)
        {
            if ((LevelState == LevelStates.play || LevelState == LevelStates.resume) && _board.GetPath.Count > 1 && _board.CanDrawPath)
            {
                _board.CanDrawPath = false;
                if (IsCorrectPath(_board.GetPath))
                    CheckPath(_board.GetPath);
                else
                {
                    _line.enabled = false;
                    _board.ClearPath();
                    counter = 0;
                    _board.CanDrawPath = true;
                    //StartCoroutine(StepEndRoutine("game_notify_no_path", false));
                }
            }

        }

        public void EndTutorial()
        {
            Pause();
            _board.gameObject.SetActive(false);
            _introEndMassagePanel.Show();
        }


        public void Pause()
        {
            Paused = true;
            LeanTween.pauseAll();
        }

        public void Resume()
        {
            Paused = false;
            LeanTween.resumeAll();
        }
        private void DrawPath(YTouchEventArgs touchEventArgs)
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            pointerData.position = touchEventArgs.TouchStart;
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);
            if (results != null && results.Count <= 0) { return; }
            Tile tile = results[0].gameObject.transform.parent.GetComponent<Tile>();
            if (tile != null)
            {
                if (!_board.PathContains(tile) && (tile.tileType == TileType.GOOD || tile.tileType == TileType.WRONG || tile.tileType == TileType.END))
                {
                    if (_board.GetPath.Count == 0) { _board.AddTile(tile); }

                    if (_board.GetPath[0] != _board.GetStartTile) { _board.ClearPath(); return; }

                    if (_board.GetPath.Count > 0)
                    {
                        if (_board.GetPath[_board.GetPath.Count - 1].neighbours.Contains(tile))
                        {
                            _board.AddTile(tile);
                        }
                    }
                    if (_board.GetPath.Count > 1)
                    {
                        _line.Points = _board.GetPath.GetPoints();
                        _line.enabled = true;
                    }
                }
            }
        }

        private bool IsCorrectPath(List<Tile> path)
        {
            if (path[0] == _board.GetStartTile) // && path[path.Count - 1] == _board.GetEndTile)
            {
                return true;
            }
            return false;
        }

        private void CheckPath(List<Tile> path)
        {
            _swipeIndicator.StopSwipeTween();
            _timer.PauseTimer();
            counter = 0;
            pathChecking = true;
            _board.tramp.GetComponent<Animator>().SetBool("walk", true);
        }

        private void SetWrongTile(Tile tile)
        {
            wrongTile = tile;
        }

        private IEnumerator StepEndRoutine(string notificationType, bool correct, int tutorial)
        {
            yield return new WaitForSeconds(0.25f);
            if (correct)
            {
                if (tutorialType == 3) { TutorialEnd = true; }
                SFXManager.Instance.PlaySFX(SFXTrack.Correct);
                _tweenM = LeanTween.scale(_notifications.CorrectIndicator, Vector3.one, 1f)
                    .setEase(LeanTweenType.easeInOutSine)
                    .setOnComplete(() =>
                    {
                        StopCoroutine("StepEndRoutine");
                        LevelManager.Instance.coroutineStarter.StartCoroutine(LevelManager.Instance.coroutineStarter.HideTutorialIndicator(_notifications.CorrectIndicator, this));
                    });
                yield return new WaitForSeconds(3.5f);
                switch (tutorial)
                {
                    case 1:
                        StartTutorialTwo();
                        break;
                    case 2:
                        StartTutorialThree();
                        break;
                    case 3:
                        EndTutorial();
                        break;
                }
            }
            else
            {
                SFXManager.Instance.PlaySFX(SFXTrack.Incorrect);
                _questionPanel.SetQuestionText(_game.LocalizedStrings[notificationType]);
                _tweenM = LeanTween.scale(_notifications.IncorrectIndicator, Vector3.one, 1f)
                    .setEase(LeanTweenType.easeInOutSine)
                    .setOnComplete(() =>
                    {
                        StopCoroutine("StepEndRoutine");
                        LevelManager.Instance.coroutineStarter.StartCoroutine(LevelManager.Instance.coroutineStarter.HideTutorialIndicator(_notifications.IncorrectIndicator, this, true));
                    });
                yield return new WaitForSeconds(3.5f);
                RerunTutorial(tutorial);
            }
        }

        private void RerunTutorial(int tutorialNumber)
        {
            switch (tutorialNumber)
            {
                case 1:
                    StartTutorialOne();
                    break;
                case 2:
                    StartTutorialTwo();
                    break;
                case 3:
                    StartTutorialThree();
                    break;
                default:
                    StartTutorialOne();
                    break;
            }
        }

        public void disableMenu(bool value)
        {
            menuButton.interactable = !value;
        }

        private void OnCorrectTouch()
        {
            Debug.Log("Correct touch detected");
            if (OnCorrectInput != null)
            {
                OnCorrectInput();
            }
        }

        private void OnIncorrectTouch()
        {
            Debug.Log("Incorrect touch detected");
            if (OnIncorrectInput != null)
            {
                OnIncorrectInput();
            }
        }
    }
}
