﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Habtic.Games.MemoryPath
{
    public class TutorialLifesHUD : MonoBehaviour
    {
        [SerializeField] private GameObject _lifePrefab;
        [SerializeField] private Sprite[] _lifeSprites;

        private List<GameObject> _lifeInstances = new List<GameObject>();
        private int _totalLifes = 0;


        public void Initialize(int totalLifes)
        {
            _totalLifes = totalLifes;
            while (_lifeInstances.Count < _totalLifes)
            {
                GameObject go = Instantiate(_lifePrefab, this.transform);
                _lifeInstances.Add(go);
            }

            for (int i = 0; i < _lifeInstances.Count; i++)
            {
                if (i < totalLifes)
                {
                    _lifeInstances[i].SetActive(true);
                    _lifeInstances[i].GetComponent<Image>().sprite = _lifeSprites[0];
                }
                else
                {
                    _lifeInstances[i].SetActive(false);
                }
            }
        }

        public void AddLifes(int lifes)
        {
            UpdateLifes(lifes);
        }

        private void UpdateLifes(int lifes)
        {
            int i = 0;
            for (; i < lifes; i++)
            {
                _lifeInstances[i].GetComponent<Image>().sprite = _lifeSprites[0];
            }

            for (; i < _totalLifes; i++)
            {
                _lifeInstances[i].GetComponent<Image>().sprite = _lifeSprites[1];
            }
        }
    }
}
