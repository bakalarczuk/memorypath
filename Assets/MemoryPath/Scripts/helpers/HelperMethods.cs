﻿using System.Collections.Generic;
using UnityEngine;

namespace Habtic.Games.MemoryPath
{
    public static class HelperMethods
    {
        public static bool CoinFlip()
        {
            return Random.Range(0f, 1f) < 0.5f;
        }

        public static Quaternion DirectionToRotation(Vector3 direction)
        {
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            return Quaternion.AngleAxis(angle, Vector3.forward);
        }

        public static Vector3 GetSpawnPointLeftOrRight()
        {
            float offset = ScreenBounds.Instance.Right / 2;
            float x = CoinFlip() ? ScreenBounds.Instance.Left - offset : ScreenBounds.Instance.Right + offset;
            float y = Random.Range(ScreenBounds.Instance.Bottom, ScreenBounds.Instance.Top);
            return new Vector2(x, y);
        }

        public static void ShowDebug(object debugObject, string className)
        {
            if (Debug.isDebugBuild) Debug.Log(className + ", Message: " + debugObject);
        }

        public static void DestroyAllChildren(this Transform transform, Transform trash = null)
        {
            List<Transform> childrenList = new List<Transform>();
            foreach (Transform child in transform)
            {
                childrenList.Add(child);
            }
            transform.DetachChildren();
            if (trash != null)
            {
                foreach (Transform child in childrenList)
                {
                    child.SetParent(trash);
                    GameObject.Destroy(child.gameObject);
                }
            }
            else
            {
                childrenList.ForEach(child => GameObject.Destroy(child.gameObject));
            }
        }

        public static Vector2[] GetPoints(this List<Tile> path)
        {
            List<Vector2> points = new List<Vector2>();
            for (int i = 0; i < path.Count; i++)
            {
                points.Add(path[i].transform.localPosition);
            }

            Vector2[] p = points.ToArray();

            return p;
        }

        public static Vector2[] GetPointsReversed(this List<Tile> path)
        {
            List<Vector2> points = new List<Vector2>();
            for (int i = path.Count - 1; i > 0; i--)
            {
                points.Add(path[i].transform.localPosition);
            }

            Vector2[] p = points.ToArray();

            return p;
        }

    }
}
