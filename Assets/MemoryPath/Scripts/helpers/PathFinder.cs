﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Habtic.Games.MemoryPath;

public class PathFinder : Singleton<PathFinder>
{

    public List<Tile> currentPath;
    public void GetPath(Tile from, Tile to)
	{
		currentPath = new List<Tile>();
		var currentNode =  from;
		var endNode = to;
		if (currentNode == null || endNode == null || currentNode == endNode)
			return;
		var openList = new List<Tile> ();
		var closedList = new List<Tile> ();
		openList.Add (currentNode);
		currentNode.previous = null;
		currentNode.distance = 0f;
		while (openList.Count > 0)
		{
			currentNode = openList[0];
			openList.RemoveAt (0);
			var dist = currentNode.distance;
			closedList.Add (currentNode);
			if (currentNode == endNode)
			{
				break;
			}
			foreach (var neighbor in currentNode.neighbours)
			{
				if (closedList.Contains (neighbor) || openList.Contains (neighbor))
					continue;
				neighbor.previous = currentNode;
				neighbor.distance = dist + (neighbor.transform.position - currentNode.transform.position).magnitude;
				var distanceToTarget = (neighbor.transform.position - endNode.transform.position).magnitude;
				openList.Add (neighbor);
			}
		}
		if (currentNode == endNode)
		{
			while (currentNode.previous != null)
			{
				currentPath.Add (currentNode);
                //currentNode.SetColor(new Color32(0,255,255,255));
				currentNode = currentNode.previous;
			}
			currentPath.Add (currentNode);
		}
	}
    public List<Tile> GetPath(Tile from, Tile to, bool withWrong)
	{
		List<Tile> thisPath = new List<Tile>();
		var currentNode =  from;
		var endNode = to;
		if (currentNode == null || endNode == null || currentNode == endNode)
			return null;
		var openList = new List<Tile> ();
		var closedList = new List<Tile> ();
		openList.Add (currentNode);
		currentNode.previous = null;
		currentNode.distance = 0f;
		while (openList.Count > 0)
		{
			currentNode = openList[0];
			openList.RemoveAt (0);
			var dist = currentNode.distance;
			closedList.Add (currentNode);
			if (currentNode == endNode)
			{
				break;
			}
			foreach (var neighbor in currentNode.neighbours)
			{
				if(withWrong){
				if (closedList.Contains (neighbor) || openList.Contains (neighbor))
					continue;
				}else{
				if (closedList.Contains (neighbor) || openList.Contains (neighbor) || currentNode.tileType == TileType.WRONG)
					continue;
				}
				neighbor.previous = currentNode;
				neighbor.distance = dist + (neighbor.transform.position - currentNode.transform.position).magnitude;
				var distanceToTarget = (neighbor.transform.position - endNode.transform.position).magnitude;
				openList.Add (neighbor);
			}
		}
		if (currentNode == endNode)
		{
			while (currentNode.previous != null)
			{
				thisPath.Add (currentNode);
                //currentNode.SetColor(new Color32(0,255,255,255));
				currentNode = currentNode.previous;
			}
			thisPath.Add (currentNode);
		}
		return thisPath;
	}
}
