﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Habtic.Games.MemoryPath
{
    public class PopupInfo : Singleton<PopupInfo>
    {

        LTDescr _tween;
        bool showPopup = true;
        public GameObject popup;

        private void Start()
        {
            showPopup = DataHolder.ShowLevelMessage;
            GameManager.OnGameSetup += OnLevelSetup;
        }

        private void OnEnable()
        {
            ResetPopup();
        }

        private void OnDestroy()
        {
            GameManager.OnGameSetup -= OnLevelSetup;
        }

        public void valueChanged()
        {
            showPopup = !showPopup;
            DataHolder.ShowLevelMessage = showPopup;
        }

        public void OnLevelSetup()
        {
            showPopup = DataHolder.ShowLevelMessage;
            if (showPopup)
            {
                popup.SetActive(true);
                GameManager.Instance.disableMenu(true);
                GameManager.Instance.Pause();
            }
            else
            {
                popup.SetActive(false);
                GameManager.Instance.Resume();
                GameManager.Instance.disableMenu(false);
                GameManager.Instance.LevelState = LevelStates.preview;
            }
        }

        public void Continue()
        {
            GameManager.Instance.disableMenu(false);
            GameManager.Instance.LevelState = LevelStates.preview;
            LeanTween.cancel(popup);
            LeanTween.moveY(popup, transform.position.y - 10, 0.5f).setEase(LeanTweenType.easeInSine).setOnComplete(() =>
            {
                popup.SetActive(false);
                GameManager.Instance.Resume();
                GameManager.Instance.LevelStart();
            });
        }

        public void ResetPopup()
        {
            LeanTween.cancel(popup);
            LeanTween.moveY(popup, transform.position.y - 1.98f, 0f);
        }

        public void ContinueIntroLevel()
        {
            // IntroLevelManager.Instance.disableMenu(false);
            IntroLevelManager.Instance.LevelState = LevelStates.preview;
            LeanTween.cancel(popup);
            LeanTween.moveY(popup, transform.position.y - 10, 0.5f).setEase(LeanTweenType.easeInSine).setOnComplete(() =>
            {
                popup.SetActive(false);
                IntroLevelManager.Instance.Resume();
                IntroLevelManager.Instance.LevelStart();
            });
        }
    }
}
