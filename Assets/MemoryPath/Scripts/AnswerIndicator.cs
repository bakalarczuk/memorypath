﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Habtic.Games.MemoryPath
{
    public class AnswerIndicator : MonoBehaviour
    {
        [SerializeField]
        private Game _game;

        [SerializeField]
        private GameObject _correctIndicator;
        [SerializeField]
        private GameObject _incorrectIndicator;
        [SerializeField]

        public GameObject CorrectIndicator { get { return _correctIndicator; } }
        public GameObject IncorrectIndicator { get { return _incorrectIndicator; } }

        private LTDescr _tweenM;

        private void OnEnable()
        {
            GameManager.OnLevelStateChanged += ShowAnswerIndicator;
            GameManager.OnCorrectInput += ShowCorrectIndicator;
            GameManager.OnIncorrectInput += ShowIncorrectIndicator;
            _correctIndicator.transform.localScale = Vector3.zero;
            _incorrectIndicator.transform.localScale = Vector3.zero;
        }

        private void OnDisable()
        {
            GameManager.OnLevelStateChanged -= ShowAnswerIndicator;
            GameManager.OnCorrectInput -= ShowCorrectIndicator;
            GameManager.OnIncorrectInput -= ShowIncorrectIndicator;
        }

        private void ShowAnswerIndicator(LevelStates levelState)
        {
            switch (levelState)
            {
                case LevelStates.setup:
                    StopAllTweens();
                    break;
                default:
                    break;
            }
        }

        private void ShowCorrectIndicator()
        {
            StopAllTweens();
            _tweenM = LeanTween.scale(_correctIndicator, Vector3.one, 0.5f)
                .setEase(LeanTweenType.easeInOutSine)
                .setOnComplete(() =>
                {
                    GameManager.Instance.GameBoard.ClearPath();
                    if (this.gameObject.activeSelf)
                        LevelManager.Instance.coroutineStarter.StartCoroutine(LevelManager.Instance.coroutineStarter.HideIndicator(_correctIndicator));
                });
            SFXManager.Instance.PlaySFX(SFXTrack.Correct);
        }

        private void ShowIncorrectIndicator()
        {
            StopAllTweens();
            _tweenM = LeanTween.scale(_incorrectIndicator, Vector3.one, 0.5f)
                .setEase(LeanTweenType.easeInOutSine)
                .setOnComplete(() =>
                {
                    GameManager.Instance.GameBoard.ClearPath();
                    if (this.gameObject.activeSelf)
                        LevelManager.Instance.coroutineStarter.StartCoroutine(LevelManager.Instance.coroutineStarter.HideIndicator(_incorrectIndicator));
                });
            SFXManager.Instance.PlaySFX(SFXTrack.Incorrect);
        }

        private void StopAllTweens()
        {
            if (_tweenM != null)
            {
                LeanTween.cancel(_tweenM.id);
                _correctIndicator.transform.localScale = Vector3.zero;
                _incorrectIndicator.transform.localScale = Vector3.zero;
            }
        }
    }
}
