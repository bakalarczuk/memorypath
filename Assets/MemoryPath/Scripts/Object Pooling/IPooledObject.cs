﻿
namespace Habtic.Games.MemoryPath
{
    public interface IPooledObject
    {

        string PoolTag { get; set; }

        void OnObjectSpawn();
        void ReEnqueueObject();
    }
}
