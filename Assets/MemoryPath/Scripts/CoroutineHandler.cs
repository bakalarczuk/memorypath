﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Habtic.Games.MemoryPath
{
    public class CoroutineHandler : MonoBehaviour
    {
        public IEnumerator HideIndicator(GameObject indicator)
        {
            yield return new WaitForSeconds(1f);
            LeanTween.scale(indicator, Vector3.zero, 0.5f)
                .setEase(LeanTweenType.easeInOutSine)
                .setOnComplete(() =>
                {
                });
            yield return new WaitForSeconds(0.5f);
            if (GameManager.Instance._level.ChallengeCounter < GameManager.Instance._level.TotalChallenges)
                GameManager.Instance.LevelStart();
            else GameManager.Instance.GameBoard.ResetBoard();

        }

        public IEnumerator HideTutorialIndicator(GameObject indicator, IntroLevelManager tutorial, bool again = false)
        {
            yield return new WaitForSeconds(1f);
            LeanTween.scale(indicator, Vector3.zero, 0.5f)
                .setEase(LeanTweenType.easeInOutSine)
                .setOnComplete(() =>
                {
                    tutorial.LevelState = LevelStates.preview;
                });

        }
    }
}
