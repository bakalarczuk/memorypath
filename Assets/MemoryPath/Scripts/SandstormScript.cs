﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Habtic.Managers;

namespace Habtic.Games.MemoryPath
{
    [ExecuteInEditMode]
    public class SandstormScript : MonoBehaviour
    {
        public delegate void SandStormStart();
        public static event SandStormStart OnSandStormStart;
        public delegate void SandStormHalf();
        public static event SandStormHalf OnSandStormHalfWay;
        public delegate void SandStormStop();
        public static event SandStormStop OnSandStormStop;

        public float startValue;
        public float stopValue;

        [SerializeField]
        private ParticleSystem rockParticles;
        [SerializeField]
        private ParticleSystem sandParticles;

        private Image image;

        private SFXClip stormAudio;

        void Awake()
        {
            PlayParticles(false);
        }

        private void PlayParticles(bool on)
        {
            if (on) SFXManager.Instance.PlaySFX(SFXTrack.Wind);
            var rocks = rockParticles.emission;
            var sand = sandParticles.emission;
            rocks.enabled = on;
            sand.enabled = on;
        }

        // Update is called once per frame
        void Start()
        {
            image = GetComponent<Image>();
            LeanTween.moveLocalX(gameObject, startValue, 0);
        }

        public void StartStorm(Animator animator, float speed)
        {
            OnSandStormStart?.Invoke();
            StartCoroutine(StartStormCycle(animator, speed));
        }

        IEnumerator StartStormCycle(Animator animator, float speed)
        {
            while (GameManager.Instance.Paused)
            {
                yield return null;
            }

            yield return new WaitForEndOfFrame();
            PlayParticles(true);
            animator.speed = speed;
            yield return new WaitForSeconds(2);
            MoveStorm();
        }

        private void MoveStorm()
        {
            SFXManager.Instance.PlaySFX(SFXTrack.Sandstorm);
            GameManager.Instance.menuButton.interactable = false;
            LTDescr id = LeanTween.moveLocalX(gameObject, stopValue, 2)
                .setOnComplete(() =>
                {
                    OnSandStormHalfWay?.Invoke();
                    PlayParticles(false);
                    LeanTween.moveLocalX(gameObject, -startValue, 2)
                    .setOnComplete(() =>
                    {
                        LeanTween.moveLocalX(gameObject, startValue, 0)
                    .setDelay(0.5f)
                    .setOnComplete(() =>
                        {
                            GameManager.Instance.menuButton.interactable = true;
                            OnSandStormStop?.Invoke();
                        });
                    });
                });
        }

        public void StopStorm()
        {
            LeanTween.moveLocalX(gameObject, startValue, 0);

        }
    }
}
