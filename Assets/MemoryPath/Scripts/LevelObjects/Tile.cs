﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Habtic.Games.MemoryPath;

    public enum TileType{
        NEUTRAL,
        WRONG,
        GOOD,
        CORNER,
        SIDE,
        PIT,
        END
    }

public class Tile : MonoBehaviour
{
    public TileType tileType = TileType.NEUTRAL;

    [SerializeField]
    private Image tileImage;
    [SerializeField]
    private Image bgImage;

    public Image BgImage{get{return bgImage;} set{bgImage = value;}}

    [SerializeField]
    private Sprite[] badTiles;

    [SerializeField]
    private Sprite[] goodTiles;

    [SerializeField]
    private Sprite[] specialTiles;

    [SerializeField]
    private Sprite pitTile;

    public Sprite sandLayerTile;

    public Sprite cornerTile;
    
    public Sprite sideTile;

    private RectTransform rectTransform;

    public List<Tile> neighbours;

    public Tile previous;

    public float distance;

    private void Awake(){
        rectTransform = gameObject.GetComponent<RectTransform>();
    }

    public void SetTile(TileType type, float angle = 0){
        tileType = type;

        Sprite[] allTileSprites = goodTiles.Concat(specialTiles).ToArray();

        if(tileType == TileType.GOOD)
            tileImage.sprite =(GameManager.Instance.GameBoard.BoardSize == GameManager.Instance.GameBoard.MaxBoardSize ? allTileSprites[Random.Range(0, allTileSprites.Length)] : goodTiles[Random.Range(0, goodTiles.Length)]);
        else if(tileType == TileType.WRONG)
            tileImage.sprite = badTiles[Random.Range(0, badTiles.Length)];
        else if(tileType == TileType.CORNER){
            tileImage.sprite = cornerTile;
            bgImage.color = new Color(0,0,0,0);
            rectTransform.localEulerAngles = new Vector3(0,0,angle);
        }
        else if(tileType == TileType.SIDE){
            tileImage.sprite = sideTile;
            bgImage.color = new Color(0,0,0,0);
            rectTransform.localEulerAngles = new Vector3(0,0,angle);
        }
        else if(tileType == TileType.PIT)
            tileImage.sprite = pitTile;

        else
            tileImage.sprite = null;
    }

    public void SetColor(Color32 color){
        tileImage.color = color;
    }

    public void ResetTile(){
        tileImage.sprite = sandLayerTile;
    }
}
