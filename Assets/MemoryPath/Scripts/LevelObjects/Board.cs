﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Habtic.Managers;
using TMPro;

namespace Habtic.Games.MemoryPath
{
    public class Board : MonoBehaviour
    {
        #region Variables and Properties

        [SerializeField]
        private CanvasScaler canvas;

        [HideInInspector]
        public int tileSize;

        [SerializeField]
        private GameObject _tilePrefab;

        [SerializeField]
        private Vector2 _boardSize;

        private Vector2 maxBoardSize = new Vector2(6, 11);

        public Vector2[] boardSizes = new Vector2[10]{
            new Vector2(4,4),
            new Vector2(5,4),
            new Vector2(5,5),
            new Vector2(6,5),
            new Vector2(6,6),
            new Vector2(6,7),
            new Vector2(6,8),
            new Vector2(6,9),
            new Vector2(6,10),
            new Vector2(6,11)
        };

        [SerializeField]
        private List<Sprite> sandLayerTiles;
        [SerializeField]
        private List<Sprite> tileBgTiles;

        private Level _level;

        private LTDescr _tweenM;

        [SerializeField]
        private List<Tile> allTiles;

        public Tile[,] tileArray;

        [SerializeField]
        private List<Tile> goodTiles;

        [SerializeField]
        private List<Tile> badTiles;

        public List<Tile> BadTiles { get { return badTiles; } }

        [SerializeField]
        private List<Tile> drawedPath;
        [SerializeField]
        private Tile startTile;
        [SerializeField]
        private Tile endTile;

        [SerializeField]
        private GameObject _templateTramp;

        [SerializeField]
        private GameObject _snakePrefab;

        [SerializeField]
        private GameObject _treasure;

        public GameObject tramp;
        public GameObject treasure;

        private bool hasSnakes = false;

        public List<Snake> snakes;

        #endregion
        #region Unity Methods

        public Tile GetStartTile { get { return startTile; } set { startTile = value; } }
        public Tile GetEndTile { get { return endTile; } }
        public List<Tile> GetPath { get { return drawedPath; } }
        public List<Tile> AllTiles { get { return allTiles; } }
        public Vector2 BoardSize { get { return _boardSize; } set { _boardSize = value; } }
        public Vector2 MaxBoardSize { get { return maxBoardSize; } set { maxBoardSize = value; } }

        public bool CanDrawPath { get; set; }

        public bool SnakesCanMove;

        void Awake()
        {
            _level = Level.Instance;
        }



        #endregion

        #region Methods

        public void ResetBoard()
        {
            ClearTiles();
            ClearPath();
            transform.DestroyAllChildren();
        }

        public void RemoveSnakes()
        {
            foreach (Snake s in snakes)
            {
                Destroy(s.gameObject);
            }
            snakes.Clear();
        }

        void ClearTiles()
        {
            allTiles.Clear();
            goodTiles.Clear();
            badTiles.Clear();
            startTile = null;
            endTile = null;
        }

        public void StartNewLevel(Level lvl, bool tutorial = false)
        {
            SnakesCanMove = true;
            ResetBoard();
            StartCoroutine(MakeGame(tutorial));
        }

        IEnumerator MakeGame(bool tutorial)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(0.5f);
            CreateGameGrid();
            CheckForNeighbors();
            AssignTiles();
            AssingStartEnd(tutorial);
            yield return new WaitForSeconds(0.2f);
            if (!tutorial) { MakePath(); }
            DoSnakes();
            PlaceTrampAndTreasure(startTile, endTile);
            MakeMines(tutorial);
        }
        public List<int> myNumbers;
        private void MakePath()
        {
            List<Tile> path = new List<Tile>();
            List<Tile> path2 = new List<Tile>();
            int pointCount = 0;
            switch (_level.Difficulty)
            {
                case LevelDifficulty.EASY:
                    pointCount = 2;
                    break;
                case LevelDifficulty.MEDIUM:
                    pointCount = 3;
                    break;
                case LevelDifficulty.HARD:
                    pointCount = 4;
                    break;
                default:
                    pointCount = 2;
                    break;
            }

            myNumbers = new List<int>();
            System.Random randNum = new System.Random();
            for (int currIndex = 0; currIndex < pointCount; currIndex++)
            {
                // generate a candidate
                int randCandidate = randNum.Next(0, goodTiles.Count);

                // generate a new candidate as long as we don't get one that isn't in the array
                while (myNumbers.Contains(randCandidate) || goodTiles[randCandidate] == startTile || goodTiles[randCandidate] == endTile)
                {
                    randCandidate = randNum.Next(0, goodTiles.Count);
                }

                myNumbers.Add(randCandidate);
            }

            ProcessList(PathFinder.Instance.GetPath(goodTiles[myNumbers[myNumbers.Count - 1]], endTile, false), path);
            if (myNumbers.Count > 1)
            {
                for (int i = myNumbers.Count - 1; i > 0; i--)
                {
                    ProcessList(PathFinder.Instance.GetPath(goodTiles[myNumbers[i - 1]], goodTiles[myNumbers[i]], false), path);
                }
            }
            ProcessList(PathFinder.Instance.GetPath(startTile, goodTiles[myNumbers[0]], false), path);

            PathFinder.Instance.currentPath = path;
        }

        private void ProcessList(List<Tile> fromList, List<Tile> toList)
        {
            for (int i = 0; i < fromList.Count; i++)
            {
                toList.Add(fromList[i]);
            }
        }

        public bool PathContains(Tile tile)
        {
            return drawedPath.Contains(tile);
        }

        public void ClearPath()
        {
            drawedPath.Clear();
        }

        public void AddTile(Tile tile)
        {
            drawedPath.Add(tile);
        }

        private void CreateGameGrid()
        {
            tileSize = (int)Mathf.Abs((transform.GetComponent<RectTransform>().rect.width * canvas.scaleFactor) / 8);
            if (_boardSize.x > maxBoardSize.x)
                _boardSize.x = maxBoardSize.x;
            if (_boardSize.y > maxBoardSize.y)
                _boardSize.y = maxBoardSize.y;

            float tileXStart = (-(tileSize * _boardSize.x) / 2) + (tileSize / 2);
            float tileYStart = (-(tileSize * _boardSize.y) / 2) - (tileSize / 2);

            tileArray = new Tile[(int)_boardSize.x, (int)_boardSize.y];

            for (int x = 0; x < _boardSize.x; x++)
            {
                for (int y = 0; y < _boardSize.y; y++)
                {
                    GameObject tile = Instantiate(_tilePrefab, transform, false) as GameObject;
                    tile.transform.GetComponent<RectTransform>().localPosition = new Vector3(tileXStart + (tileSize * x), tileYStart + (tileSize * y), -200);
                    tile.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(tileSize, tileSize);
                    Tile _tile = tile.GetComponent<Tile>();
                    _tile.SetTile(TileType.GOOD);
                    allTiles.Add(_tile);
                    tileArray[x, y] = _tile;
                    tileArray[x, y].GetComponent<Tile>().sandLayerTile = sandLayerTiles.Where(obj => obj.name == "s-" + (y + 1) + "-" + (x + 1)).SingleOrDefault();
                    tileArray[x, y].GetComponent<Tile>().BgImage.sprite = tileBgTiles.Where(obj => obj.name == "b" + (y + 1) + "x" + (x + 1)).SingleOrDefault();
                }
            }

            for (int r = 0; r < _boardSize.x + 2; r++)
            {
                GameObject tile1 = Instantiate(_tilePrefab, transform, false) as GameObject;
                GameObject tile2 = Instantiate(_tilePrefab, transform, false) as GameObject;
                tile1.transform.GetComponent<RectTransform>().localPosition = new Vector3(tileXStart + (tileSize * r) - tileSize, tileYStart + (tileSize * _boardSize.y), -200);
                tile1.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(tileSize, tileSize);
                Tile _tile1 = tile1.GetComponent<Tile>();
                _tile1.SetTile((r == 0 ? TileType.CORNER : (r == _boardSize.x + 1 ? TileType.CORNER : TileType.SIDE)), (r == 0 ? 270 : (r == _boardSize.x + 1 ? 180 : 180)));
                tile2.transform.GetComponent<RectTransform>().localPosition = new Vector3(tileXStart + (tileSize * r) - tileSize, tileYStart - (tileSize), -200);
                tile2.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(tileSize, tileSize);
                Tile _tile2 = tile2.GetComponent<Tile>();
                _tile2.SetTile((r == 0 ? TileType.CORNER : (r == _boardSize.x + 1 ? TileType.CORNER : TileType.SIDE)), (r == 0 ? 0 : (r == _boardSize.x + 1 ? 90 : 0)));
            }

            for (int q = 0; q < _boardSize.y; q++)
            {
                GameObject tile1 = Instantiate(_tilePrefab, transform, false) as GameObject;
                GameObject tile2 = Instantiate(_tilePrefab, transform, false) as GameObject;
                tile1.transform.GetComponent<RectTransform>().localPosition = new Vector3(tileXStart - (tileSize), tileYStart + (tileSize * q), -200);
                tile1.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(tileSize, tileSize);
                Tile _tile1 = tile1.GetComponent<Tile>();
                _tile1.SetTile(TileType.SIDE, 270);
                tile2.transform.GetComponent<RectTransform>().localPosition = new Vector3(tileXStart + (tileSize * _boardSize.x), tileYStart + (tileSize * q), -200);
                tile2.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(tileSize, tileSize);
                Tile _tile2 = tile2.GetComponent<Tile>();
                _tile2.SetTile(TileType.SIDE, 90);
            }

        }

        private void CheckForNeighbors()
        {
            for (int x = 0; x < _boardSize.x; x++)
            {
                for (int y = 0; y < _boardSize.y; y++)
                {
                    //Left neighbor
                    if (x - 1 >= 0)
                    {
                        if (tileArray[x - 1, y].GetComponent<Tile>().tileType == TileType.GOOD)
                            tileArray[x, y].GetComponent<Tile>().neighbours.Add(tileArray[x - 1, y]);
                    }
                    //Right neighbor
                    if (x + 1 < _boardSize.x)
                    {
                        if (tileArray[x + 1, y].GetComponent<Tile>().tileType == TileType.GOOD)
                            tileArray[x, y].GetComponent<Tile>().neighbours.Add(tileArray[x + 1, y]);
                    }
                    //Below neighbor
                    if (y - 1 >= 0)

                    {
                        if (tileArray[x, y - 1].GetComponent<Tile>().tileType == TileType.GOOD)
                            tileArray[x, y].GetComponent<Tile>().neighbours.Add(tileArray[x, y - 1]);
                    }
                    //Above neighbor
                    if (y + 1 < _boardSize.y)
                    {
                        if (tileArray[x, y + 1].GetComponent<Tile>().tileType == TileType.GOOD)
                            tileArray[x, y].GetComponent<Tile>().neighbours.Add(tileArray[x, y + 1]);
                    }
                }
            }
        }

        private void AssignTiles()
        {
            goodTiles.AddRange(allTiles);
        }

        private void MakeMines(bool tutorial = false)
        {
            if (tutorial)
            {
                Tile wrongTile = goodTiles[goodTiles.Count / 2];
                goodTiles.Remove(wrongTile);
                badTiles.Add(wrongTile);
                wrongTile.SetTile(TileType.WRONG);
            }
            else
            {
                List<int> myNumbers = new List<int>();
                System.Random randNum = new System.Random();

                foreach (Tile t in GetEndTile.neighbours)
                {
                    if (!PathFinder.Instance.currentPath.Contains(t) && _level.WrongTilesCount >= 8)
                    {
                        myNumbers.Add(goodTiles.IndexOf(t));
                        goodTiles.Remove(t);
                        badTiles.Add(t);
                        t.SetTile(TileType.WRONG);

                    }
                }

                for (int currIndex = 0; currIndex < _level.WrongTilesCount; currIndex++)
                {
                    // generate a candidate
                    int randCandidate = randNum.Next(0, goodTiles.Count);

                    // generate a new candidate as long as we don't get one that isn't in the array
                    while (myNumbers.Contains(randCandidate) || PathFinder.Instance.currentPath.Contains(goodTiles[randCandidate]))
                    {
                        randCandidate = randNum.Next(0, goodTiles.Count);
                    }

                    myNumbers.Add(randCandidate);

                    Tile currentTile = goodTiles[randCandidate];

                    goodTiles.Remove(currentTile);
                    badTiles.Add(currentTile);
                    currentTile.SetTile(TileType.WRONG);
                }
                // _level.MaxWrongTiles = (int)(allTiles.Count - PathFinder.Instance.currentPath.Count);
            }
        }

        private void AssingStartEnd(bool tutorial = false)
        {
            if (!tutorial)
            {
                int factor = (int)(_boardSize.x / 3);

                startTile = goodTiles[Random.Range(0, (int)_boardSize.y) * factor];
                endTile = goodTiles[Random.Range(goodTiles.Count - ((int)_boardSize.y * factor), goodTiles.Count)];
            }
            else
            {
                startTile = goodTiles[0];
                endTile = goodTiles[goodTiles.Count - 1];
            }
            endTile.tileType = TileType.END;
        }

        public void PlaceTrampAndTreasure(Tile startTile, Tile endTile)
        {
            GameObject tramp = Instantiate(_templateTramp, transform, false) as GameObject;
            GameObject treasure = Instantiate(_treasure, transform, false) as GameObject;

            JewelTile jewel = treasure.GetComponent<JewelTile>();
            jewel.SetJewel();

            this.tramp = tramp;
            this.treasure = treasure;

            tramp.transform.localPosition = startTile.transform.localPosition;
            tramp.transform.localScale = new Vector3(tileSize / 2, tileSize / 2, tileSize / 2);
            treasure.transform.localPosition = endTile.transform.localPosition;
            treasure.transform.localScale = new Vector3(tileSize / 1.22f, tileSize / 1.22f, tileSize / 1.22f);
        }

        public void DoSnakes()
        {
            if (hasSnakes) { return; }
            List<int> myNumbers = new List<int>();
            System.Random randNum = new System.Random();
            float prevAngle = 0;
            for (int i = 0; i < _level.WrongTilesCount * 3; i++)
            {
                // generate a candidate
                int randCandidate = randNum.Next(0, goodTiles.Count);

                // generate a new candidate as long as we don't get one that isn't in the array
                while (myNumbers.Contains(randCandidate))
                {
                    randCandidate = randNum.Next(0, goodTiles.Count);
                }

                myNumbers.Add(randCandidate);

                Tile currentTile = goodTiles[randCandidate];

                GameObject snake = Instantiate(_snakePrefab, currentTile.transform, false);
                Snake s = snake.GetComponent<Snake>();
                snake.transform.localPosition = Vector2.zero;
                snake.transform.localScale = new Vector3(tileSize / 2, tileSize / 2, tileSize / 2);
                snake.transform.eulerAngles = new Vector3(0, 0, prevAngle);
                s.dir = prevAngle == 0 ? Snake.SnakeDir.Up : prevAngle == 90 ? Snake.SnakeDir.Left : prevAngle == 180 ? Snake.SnakeDir.Down : prevAngle == 270 ? Snake.SnakeDir.Right : Snake.SnakeDir.Up;
                snakes.Add(s);
                prevAngle += 90;
                if (prevAngle == 360) { prevAngle = 0; }
            }

            hasSnakes = true;
        }
    }

    #endregion
}

