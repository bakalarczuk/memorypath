﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JewelTile : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer sprite;
    
    [SerializeField]
    private Sprite[] jewels;

    public void SetJewel(){
        sprite.sprite = jewels[Random.Range(0, jewels.Length)];
    }
}
