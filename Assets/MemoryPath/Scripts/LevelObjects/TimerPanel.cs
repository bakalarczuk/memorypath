﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Habtic.Games.MemoryPath
{
    public class TimerPanel : MonoBehaviour
    {
        [SerializeField] private GameObject _indicatorHolder;
        [SerializeField] private Image _indicator;

        [SerializeField] private bool tutorialMode = false;

        public void OnEnable()
        {
            SandstormScript.OnSandStormStop += OnSandStormStop;
            FlashController.OnFadeIn += TimerFinished;
            ShowIndicator(false);
        }

        public void OnDisable()
        {
            SandstormScript.OnSandStormStop -= OnSandStormStop;
            FlashController.OnFadeIn -= TimerFinished;
        }

        private void OnSandStormStop()
        {
            if (!tutorialMode) ShowIndicator(true);
        }

        private void TimerFinished()
        {
            ShowIndicator(false);
        }


        public void ShowIndicator(bool show, float amount = 0)
        {
            _indicatorHolder.SetActive(show);
            _indicator.enabled = show;
            _indicator.fillAmount = amount;
        }

        public void Hide()
        {
            _indicator.enabled = false;
        }

        public void UpdateTimer(float value)
        {
            _indicator.fillAmount = Mathf.Lerp(0.0f, 1.0f, value);
        }

    }
}