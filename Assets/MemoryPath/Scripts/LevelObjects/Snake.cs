﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{

    public enum SnakeDir
    {
        Up,
        Down,
        Left,
        Right
    }
    public SnakeDir dir;

    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
        animator.speed = 0;
    }
    public void StartMoving()
    {
        if (dir == SnakeDir.Up) { LeanTween.moveLocalY(gameObject, transform.localPosition.y + 1000, 7); }
        else if (dir == SnakeDir.Right) { LeanTween.moveLocalX(gameObject, transform.localPosition.x + 1000, 7); }
        else if (dir == SnakeDir.Left) { LeanTween.moveLocalX(gameObject, transform.localPosition.x - 1000, 7); }
        else if (dir == SnakeDir.Down) { LeanTween.moveLocalY(gameObject, transform.localPosition.y - 1000, 7); }

        animator.speed = 1;
    }
}
