﻿using UnityEngine;

namespace Habtic.Games.MemoryPath
{
    public struct LevelProgression
    {
        public float Start { get; set; }
        public int Max { get; set; }
        public float Increase { get; set; }
    }

    public enum LevelDifficulty
    {
        EASY,
        MEDIUM,
        HARD
    }

    public class Level
    {
        private static Level _instance;

        public static Level Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Level();
                }

                return _instance;
            }
        }

        #region PROPERTIES
        public int ScorePerCorrectAnswer { get; private set; }
        public int NextLevel { get; private set; }
        public int NextBoardSizeLevel { get; private set; }
        public int CorrectInARowCounter { get; set; }
        public int CorrectCounter { get; set; } = 0;
        public int IncorrectCounter { get; set; } = 0;
        public int TotalLifes { get; private set; }
        public int TotalChallenges { get; private set; }
        public int MaxWrongTiles { get; set; }
        public int ChallengeCounter { get; set; }
        public int Complexity { get; set; }
        public int WrongTilesCount { get; set; }
        public int MinesDiff { get; set; }
        public int BoardSize { get; set; }
        public LevelDifficulty Difficulty { get; private set; }
        public int CurrentLevel
        {
            get
            {
                return _currentLevel;
            }
            set
            {
                _currentLevel = value;
                UpdateLevelSettings(_currentLevel);
            }
        }

        #endregion PROPERTIES

        private int _currentLevel = 1;
        private LevelProgression _scoreProgression;
        private LevelProgression _nextLevelProgression;
        private LevelProgression _movingOutTime;

        private Level()
        {
            TotalLifes = 5;
            ChallengeCounter = 0;
            Complexity = 1;
            CorrectCounter = 0;
            IncorrectCounter = 0;
            MaxWrongTiles = 4;
            TotalChallenges = 15;
            CurrentLevel = 1;
            MinesDiff = 1;
            BoardSize = 0;
            GameManager.Instance.GameBoard.BoardSize = GameManager.Instance.GameBoard.boardSizes[BoardSize];
            UpdateLevelSettings(_currentLevel);
        }

        public void ResetLevel(Level lvl)
        {
            CurrentLevel = lvl.CurrentLevel;
            TotalLifes = lvl.TotalLifes;
            ChallengeCounter = 0;
            Complexity = lvl.Complexity;
            CorrectCounter = 0;
            IncorrectCounter = 0;
            MaxWrongTiles = lvl.MaxWrongTiles;
            TotalChallenges = lvl.TotalChallenges;
            CurrentLevel = lvl.CurrentLevel;
            MinesDiff = lvl.MinesDiff;
            BoardSize = lvl.BoardSize;
            GameManager.Instance.GameBoard.BoardSize = GameManager.Instance.GameBoard.boardSizes[BoardSize];
            UpdateLevelSettings(_currentLevel);
        }

        public void SetDifficulty(LevelDifficulty difficulty)
        {
            Difficulty = difficulty;
            switch (difficulty)
            {
                case LevelDifficulty.EASY:
                    CurrentLevel = 1;
                    MinesDiff = 1;
                    TotalLifes = 5;
                    TotalChallenges = 15;
                    MaxWrongTiles = 4;
                    BoardSize = 0;
                    WrongTilesCount = (WrongTilesCount == MaxWrongTiles ? MaxWrongTiles : _currentLevel * MinesDiff);
                    GameManager.Instance.GameBoard.BoardSize = GameManager.Instance.GameBoard.boardSizes[BoardSize];
                    break;
                case LevelDifficulty.MEDIUM:
                    CurrentLevel = 1;
                    MinesDiff = 5;
                    TotalLifes = 4;
                    TotalChallenges = 30;
                    MaxWrongTiles = 8;
                    BoardSize = 3;
                    WrongTilesCount = (WrongTilesCount == MaxWrongTiles ? MaxWrongTiles : _currentLevel * MinesDiff);
                    GameManager.Instance.GameBoard.BoardSize = GameManager.Instance.GameBoard.boardSizes[BoardSize];
                    break;
                case LevelDifficulty.HARD:
                    CurrentLevel = 1;
                    MinesDiff = 8;
                    TotalLifes = 3;
                    MaxWrongTiles = 16;
                    TotalChallenges = 60;
                    WrongTilesCount = (WrongTilesCount == MaxWrongTiles ? MaxWrongTiles : _currentLevel * MinesDiff);
                    BoardSize = 6;
                    GameManager.Instance.GameBoard.BoardSize = GameManager.Instance.GameBoard.boardSizes[BoardSize];
                    break;
                default:
                    CurrentLevel = 1;
                    MinesDiff = 1;
                    TotalLifes = 5;
                    TotalChallenges = 15;
                    MaxWrongTiles = 4;
                    BoardSize = 0;
                    WrongTilesCount = (WrongTilesCount == MaxWrongTiles ? MaxWrongTiles : _currentLevel * MinesDiff);
                    GameManager.Instance.GameBoard.BoardSize = GameManager.Instance.GameBoard.boardSizes[BoardSize];
                    break;
            }
        }

        private void UpdateLevelSettings(int level)
        {
            ScorePerCorrectAnswer = 50 * level * (Difficulty == LevelDifficulty.EASY ? 1 : Difficulty == LevelDifficulty.MEDIUM ? 2 : Difficulty == LevelDifficulty.HARD ? 4 : 1);
            WrongTilesCount = (WrongTilesCount == MaxWrongTiles ? MaxWrongTiles : level * MinesDiff);
            NextLevel = 3;
            CorrectInARowCounter = 0;
            NextBoardSizeLevel = 5;
        }
    }
}
