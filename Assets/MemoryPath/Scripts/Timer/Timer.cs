﻿using System;
using UnityEngine;
using TMPro;
using UnityEngine.Serialization;

namespace Habtic.Games.MemoryPath
{

    public class Timer : MonoBehaviour
    {
        public bool tutorialModeOn = false;
        public delegate void PercentageLeftChanged(float percentageLeft);
        public event PercentageLeftChanged OnPercentageLeftChanged;

        public delegate void TimerEventHandler();
        public event TimerEventHandler OnTimerFinished;

        #region Properties
        public float PercentageLeft
        {
            get { return _percentageLeft; }
            private set
            {
                _percentageLeft = value;
                OnPercentageLeftChanged?.Invoke(value / 100.0f);
            }
        }

        public bool Done
        {
            get
            {
                return ElapsedTime >= Duration;
            }
        }

        public bool Running { get; private set; }
        public float ElapsedTime { get; private set; }
        public float Duration { get; private set; }

        #endregion

        [FormerlySerializedAs("TimeText")] public TMP_Text _timeText;

        private float _secondsLeft;
        private float _percentageLeft;
        private int _seconds = 59;
        private int _minutes = 1;

        #region Unity Methods

        private void Update()
        {
            if (!tutorialModeOn)
            {
                if (Running && !GameManager.Instance.Paused)
                {
                    Tick();
                    if (_timeText != null)
                        UpdateTimeText();
                }
            }
            else
            {
                if (Running && !IntroLevelManager.Instance.Paused)
                {
                    Tick();
                    if (_timeText != null)
                        UpdateTimeText();
                }
            }
        }

        private void OnDisable()
        {
            if (_timeText != null)
                _timeText.text = "00:00";
        }

        #endregion

        #region Methods

        public void TimerReset()
        {
            Running = false;
            ElapsedTime = 0f;
            Duration = 0f;
            if (_timeText != null)
                ResetTimeText();
        }

        public void Tick()
        {
            if (Done)
            {
                Running = false;
                if (_timeText != null)
                    _timeText.text = "00:00";
                OnTimerFinished();
                return;
            }

            ElapsedTime += Time.deltaTime;
            _secondsLeft = Duration - ElapsedTime;
            PercentageLeft = (ElapsedTime / Duration) * 100;
        }

        public void StartTimer(float minutes, float seconds, bool autoStart = false)
        {
            float duration = (minutes * 60) + seconds;
            StartTimer(duration, autoStart);
        }

        public void StartTimer(float seconds, bool autoStart = false)
        {
            Running = autoStart;
            Duration = seconds;
            ElapsedTime = 0f;
            if (_timeText != null)
                UpdateTimeText();
        }


        public void PauseTimer()
        {
            Running = false;
        }

        public void ResumeTimer()
        {
            Running = true;
        }

        private void UpdateTimeText()
        {
            _minutes = Mathf.FloorToInt(_secondsLeft / 60);
            if (_minutes < 0)
            {
                _minutes = 0;
            }
            float sl = _secondsLeft % 60;
            if (sl > 0)
            {
                _seconds = Mathf.FloorToInt(sl);
            }
            else if (sl < 0)
            {
                _seconds = 0;
            }
            else
            {
                _seconds = 0;
            }

            _timeText.text = _minutes.ToString("00") + ":" + ((int)_seconds).ToString("00");
        }

        private void ResetTimeText()
        {
            _minutes = 0;
            _secondsLeft = 0;
            _timeText.text = _minutes.ToString("00") + ":" + ((int)_seconds).ToString("00");
        }

        #endregion
    }
}
