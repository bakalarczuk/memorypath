﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Habtic.Games.MemoryPath {
    [RequireComponent (typeof (Fade))]
    public class FlashController : Singleton<FlashController> {
        [SerializeField]
        private Game _game;
        private Fade _fade;
        public TMP_Text levelUpText;
        public GameObject Popup;

        public delegate void FadeIn ();
        public static event FadeIn OnFadeIn;
        public delegate void FadeOut ();
        public static event FadeOut OnFadeOut;

        public bool flashing = false;

        LTDescr _tween;

        private void Awake () {
            _fade = GetComponent<Fade> ();
        }

        private void OnEnable () {
            GameManager.OnNextLevel += Flash;
            IntroLevelManager.OnNewTutorial += FlashMe;
            GameManager.OnNewBoard += FlashMe;
        }

        private void OnDisable () {
            GameManager.OnNextLevel -= Flash;
            IntroLevelManager.OnNewTutorial -= FlashMe;
            GameManager.OnNewBoard -= FlashMe;
        }

        private void Flash (NextLevelEventArgs args) {
            StartCoroutine (NextLevel ());
        }

        private void FlashMe () {
            StartCoroutine (OnceFlash ());
        }

        IEnumerator NextLevel () {
            GameManager.Instance.LevelState = LevelStates.pause;
            levelUpText.text = $"{_game.LocalizedStrings["game_flash_text_level"]} {Level.Instance.CurrentLevel.ToString()}!";
            _fade.In (false, () => {
                _fade.Out ();
            });
            yield return new WaitForSeconds (0.5f);

            _tween = LeanTween.moveLocalX (Popup, 0 - (Popup.GetComponent<RectTransform> ().rect.width / 2), 1).setEase (LeanTweenType.easeOutSine);
            yield return new WaitForSeconds (1.5f);
            _tween = LeanTween.moveLocalX (Popup, -1000, 1).setEase (LeanTweenType.easeInSine);
            _tween.setOnComplete (() => {
                GameManager.Instance.LevelState = LevelStates.play;
                Popup.transform.localPosition = new Vector2 (1000, 0);
                //GameManager.Instance.AnswerTime -= GameManager.Instance.AnswerTime > 3 ? 1f : GameManager.Instance.AnswerTime > 1 ? 0.5f : 0;
                GameManager.Instance._level.Complexity += 1;
                GameManager.Instance.LevelStart ();
            });
        }

        IEnumerator OnceFlash () {
            flashing = true;
            yield return null;
            _fade.In (false, () => {
                if (OnFadeIn != null) { OnFadeIn (); }
                _fade.Out (() => {
                    if (OnFadeOut != null) { OnFadeOut (); }
                    flashing = false;
                });
            });
        }
    }
}