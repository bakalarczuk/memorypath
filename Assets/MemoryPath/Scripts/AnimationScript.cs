﻿using UnityEngine;
using UnityEngine.UI;

namespace Habtic.Games.MemoryPath
{
    public class AnimationScript : MonoBehaviour
    {
        public float framesPerSecond = 30;

        public Sprite[] sprites;
    }
}
